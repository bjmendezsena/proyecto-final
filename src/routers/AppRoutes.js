import React, {Component} from 'react';

import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import LoginScreen from '../componentes/loginComponent/LoginScreen';
import PaginaPrincipal from '../componentes/paginaPrincipal/PaginaPrincipal';
import { DashboardRoutes } from './DashBoardRoutes';


class AppRoutes extends Component {
    render() { 
        return (
            <Router>
                <div>
                    <Switch>
                    <Route exact path="/mainpage" component={PaginaPrincipal} />
                        <Route exact path ="/login" component ={LoginScreen}/>
                        <Route path ="/" component ={DashboardRoutes}/>
                    </Switch>
                </div>
            </Router>
        );
    }
}
 
export default AppRoutes;