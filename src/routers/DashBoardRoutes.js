import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
/////////////////////////////////////////////////////////////////////////////
//import DetallesClub from '../componentes/managers/model/clubs/DetallesClub';//
import Header from '../componentes/headerComponent/HeaderComponent';        //
import PrincipalManager from '../componentes/managers/PrincipalManager';
//////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////
import ListadoUsuarios from '../componentes/admins/UsuarioComponents/ListadoUsuarios';            //
import ListadoClubs from '../componentes/admins/ClubComponents/ListadoClubs';                     //
import FormularioAltaClub from '../componentes/admins/ClubComponents/FormularioAltaClub';         //
import FormularioAltaUsuario from '../componentes/admins/UsuarioComponents/FormularioAltaUsuario';
import InfoClub from '../componentes/admins/ClubComponents/InfoClub';//
import InfoUsuario from '../componentes/admins/UsuarioComponents/InfoUsuario';//
import ModificarUsuario from '../componentes/admins/UsuarioComponents/ModificarUsuario';//
import ModificarClub from '../componentes/admins/ClubComponents/ModificarClub';//
//////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////
import AltaProductos from '../componentes/managers/model/ProductosComponents/AltaProductos';//
import ListadoProductos from '../componentes/managers/model/ProductosComponents/ListadoProductos';//
import InfoProductos from '../componentes/managers/model/ProductosComponents/InfoProductos';//
import EditarProductos from '../componentes/managers/model/ProductosComponents/EditarProductos'
import ListadoReservas from '../componentes/managers/model/ReservasComponents/ListadoReservas';//
import EditarReservas from '../componentes/managers/model/ReservasComponents/EditarReservas';//
import InfoReservas from '../componentes/managers/model/ReservasComponents/InfoReservas';//
import ListadoPromociones from '../componentes/managers/model/PromocionesComponents/ListadoPromociones';//
import InfoPromociones from '../componentes/managers/model/PromocionesComponents/InfoPromociones';//
import AltaPromociones from '../componentes/managers/model/PromocionesComponents/AltaPromociones';
import EditarPromociones from '../componentes/managers/model/PromocionesComponents/EditarPromociones';
import ListadoPedidos from '../componentes/managers/model/PedidosComponents/ListadoPedidos';
import InfoPedidos from '../componentes/managers/model/PedidosComponents/InfoPedidos';
import EditarPedidos from '../componentes/managers/model/PedidosComponents/EditarPedidos';
import Error404 from '../componentes/Error404';
///////////////////////////////////////////////////////////////////////////////////////////////////////
//Footer
import PieDePagina from '../componentes/paginaPrincipal/PieDePagina';
import DetallesClub from '../componentes/managers/model/clubs/DetallesClub';
//////////////////////////////////////////
export const DashboardRoutes = (props) => {

    const userDashboard = props.history.location.state;
    
    return (
        <>
            <Header userDash = {userDashboard}/>
            <div className="container mt-2">
                <Switch>
                    
                    {/**----------------------------------------------------------------------------------*/}
                    <Route path="/principalmanagers/:idManager/club/:idClub" component={DetallesClub} />
                    <Route exact path="/principalmanagers/:idManager" component={PrincipalManager} />
                    {/**----------------------------------------------------------------------------------*/}
                    <Route exact path="/Clubs" component={ListadoClubs} />
                    <Route exact path="/Usuarios" component={ListadoUsuarios} />
                    <Route exact path="/AltaClub" component={FormularioAltaClub} />
                    <Route exact path="/AltaUsuario" component={FormularioAltaUsuario} />
                    <Route path="/InfoClub/:id" component={InfoClub} />
                    <Route path="/InfoUsuario/:id" component={InfoUsuario} />
                    <Route path="/ModificarUsuario/:id" component={ModificarUsuario} />
                    <Route path="/ModificarClub/:id" component={ModificarClub} />

                    <Route exact path="/Productos" component={ListadoProductos} />
                    <Route exact path="/AltaProductos" component={AltaProductos} />
                    <Route path="/info/:id" component={InfoProductos} />
                    <Route path="/EditarProductos/:id" component={EditarProductos} />

                    <Route exact path="/Reservas" component={ListadoReservas} />
                    <Route path="/InfoReservas/:id" component={InfoReservas} />
                    <Route path="/EditarReservas/:id" component={EditarReservas} />

                    <Route exact path="/Promociones" component={ListadoPromociones} />
                    <Route exact path="/AltaPromociones" component={AltaPromociones} />
                    <Route path="/InfoPromociones/:id" component={InfoPromociones} />
                    <Route path="/EditarPromociones/:id" component={EditarPromociones} />

                    <Route exact path="/Pedidos" component={ListadoPedidos} />
                    <Route path="/InfoPedidos/:id" component={InfoPedidos} />
                    <Route path="/EditarPedidos/:id" component={EditarPedidos} />

                    {/**-----------------------------------------------------------------------------------*/}                    
                    <Redirect to='/mainpage'></Redirect>
                    <Route component={Error404} />
                    {/**-----------------------------------------------------------------------------------*/}
                </Switch>
                <PieDePagina />
            </div>
        </>
    )
}
