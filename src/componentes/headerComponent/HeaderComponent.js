import React, { Component } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, NavbarText } from 'reactstrap';
// Imagen LOGO
import themeLogo from '../../Images/themeLogo.png';
import '../admins/estilos.css';
/*const links = [
    { href: '/Promociones', text: 'Promociones' },
    { href: '/Pedidos', text: 'Pedidos' },
    { href: '/Productos', text: 'Productos' },
    { href: '/Reservas', text: 'Reservas' },
    { href: '/Clubs', text: 'Clubs' }
];

const createNavItem = ({ href, text, className }) => (
    <NavItem >
        <NavLink href={href} className={className}>{text}</NavLink>
    </NavItem>
);*/

export default class HeaderComponent extends Component {
    constructor(props) {
        super(props);
        
        
        this.state = {
            isOpen: false,
            userHeadr: this.props.userDash
        };
       

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen,
        });
        //this.state.userHeadr.userName
        //this.state.userHeadr.mail
        
    }

    render() {
        if(this.state.userHeadr !=null){
            return (
            
                <>
                    <Navbar color="light" light expand="lg" width="100%">
                        <NavbarBrand href="/"><img src={themeLogo} alt="themeLogo" className="logoHeader"/>Wolpray</NavbarBrand>
                        {/* <ion-icon name="menu-outline"></ion-icon> */}
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink >Username: {this.state.userHeadr.userName}</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink >Email: {this.state.userHeadr.mail}</NavLink>
                            </NavItem>
                            
                            </Nav>
                            <NavbarText>
                                <NavLink href="/login">Logout</NavLink>
                            </NavbarText>
                        </Collapse>
                    </Navbar>
                </>
            );

            
        }else if(this.state.userHeadr ==null){
            return (
            
                <>
                    <Navbar color="light" light expand="lg" width="100%">
                        <NavbarBrand href="/"><img src={themeLogo} alt="themeLogo" className="logoHeader"/>Wolpray</NavbarBrand>
                        {/* <ion-icon name="menu-outline"></ion-icon> */}
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink >Username:</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink >Email:</NavLink>
                            </NavItem>
                            
                            </Nav>
                            <NavbarText>
                                <NavLink href="/login">Logout</NavLink>
                            </NavbarText>
                        </Collapse>
                    </Navbar>
                </>
            );

        }
        
        
    }
}


/*import React, { useState } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Button,
    NavbarText
} from 'reactstrap';

const HeaderComponent = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div>
            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">CLubs seleccionado</NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar>
                       
                    </Nav>
                    <NavbarText>
                        <Button color="primary"><i className="fas fa-user"></i></Button>
                    </NavbarText>
                    <NavbarText>
                        <Button color="primary">Logout</Button>
                    </NavbarText>
                </Collapse>
            </Navbar>
        </div>
    );
}

export default HeaderComponent;*/
