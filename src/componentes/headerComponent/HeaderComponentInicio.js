import React, { Component } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
// Imagen LOGO
import themeLogo from '../../Images/themeLogo.png';
import '../admins/estilos.css';

export default class HeaderComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            userHeadr: this.props.userDash
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    }

    render() {

        return (

            <>
                <Navbar color="light" light expand="lg" width="100%">
                    <NavbarBrand href="/"><img src={themeLogo} alt="themeLogo" className="logoHeader" />Wolpray</NavbarBrand>
                    {/* <ion-icon name="menu-outline"></ion-icon> */}
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href="/login" className="barraNavegacionPrincipal">Iniciar sesión </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </>
        );

    }
}
