import React, { Component } from 'react';
import Promociones from '../../../../modelos/Promociones';
import { Redirect, Link } from 'react-router-dom';
import { Input, Label, Form, Spinner } from 'reactstrap';
import "bootstrap/dist/css/bootstrap.min.css";

export default class AltaPromociones extends Component {

    constructor(props) {
        super(props);
        this.state = {
            promociones: Promociones.getPromocion(),
            idClub: props.match.params.IdClubDetalles,
            promotionId: "",
            clubId: "",
            name: "",
            description: "",
            coverUrl: "",
            altaPromociones: 0
        }
        this.promocionNueva = this.promocionNueva.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleChangeImage = this.handleChangeImage.bind(this);
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleChangeImage(evt) {
        console.log("Uploading");
        var reader = new FileReader();
        var file = evt.target.files[0];
        var self = this;

        reader.onload = function (upload) {
            self.setState({
                coverUrl: upload.target.result
            }, function () {
                console.log(self.state.imageUrl);
            });
        };
        reader.readAsDataURL(file);
    }

    promocionNueva(e) {
        e.preventDefault();
        if (this.state.name !== "") {
            this.setState({ altaPromociones: 1 });
            Promociones.addPromocion(
                this.state.idClub,
                this.state.name,
                this.state.description,
                this.state.coverUrl
            )
                .then(response => {
                    console.log(response);
                    this.setState({ altaPromociones: 2 });
                })
        } else {
            alert("No se puede dar de alta esta promoción");
        }
    }

    render() {
        const { name, description } = this.state;
        if (this.state.altaPromociones === 2) {
            return (
                <Redirect to={"../"} />
            )
        } else if (this.state.altaPromociones === 1) {

            return (<><Spinner color="primary" /> </>)
        }

        return (
            <>
                <h1 className="form-group text-center">Añadir promoción</h1>

                <div className="row">
                    <Form className="formulario" onSubmit={this.promocionNueva} method="POST">
                        <Label htmlFor="clubid"><b>Club:</b></Label>
                        <Input type="number" name="clubid" placeholder="Club" onChange={this.handleInputChange} value={this.state.idClub} disabled />
                        <Label htmlFor="name"><b>Nombre:</b></Label>
                        <Input type="text" name="name" placeholder="Nombre" onChange={this.handleInputChange} value={name} />
                        <Label htmlFor="description"><b>Descripción:</b></Label>
                        <Input type="text" name="description" placeholder="Descripción" onChange={this.handleInputChange} value={description} />
                        <Label htmlFor="coverurl"><b>Cover:</b></Label>
                        <Input type="file" name="coverUrl" placeholder="Cover" onChange={this.handleChangeImage} encType="multipart/form-data" />
                        <br />
                        <Input type="submit" value="Dar de alta" />
                        <br />
                        <Link to="../" className="link">Volver al listado</Link>
                    </Form>
                </div>
            </>

        )
    }
}
