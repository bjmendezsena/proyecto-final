import React from 'react';
import Promociones from '../../../../modelos/Promociones';
import { Button, Row, Spinner } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';

export default class InfoPromociones extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idManager: props.match.params.IdManagerCDetalles,
            idClub: props.match.params.IdClubDetalles,
            id: "",
            clubId: "",
            name: "",
            description: "",
            coverUrl: "",
            borrado: 0
        };
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Promociones.getPromocionById(this.props.match.params.id * 1)
            .then(promociones => {
                this.setState({
                    id: promociones.data.id,
                    clubId: promociones.data.clubId,
                    name: promociones.data.name,
                    description: promociones.data.description,
                    coverUrl: promociones.data.coverUrl
                });
            })
            .catch(err => console.log(err));
    }

    borrado(idBorrar) {
        if (window.confirm("Desea borrar '" + this.state.name + "'?")) {
            this.setState({ borrado: 1 })
            Promociones.deletePromocion(idBorrar)
                .then(response => {
                    console.log(response);
                    this.setState({ borrado: 2 })
                })
        }
    }

    render() {

        if (this.state.borrado === 2) {
            return (
                <Redirect to={"../"} />
            )
        }

        if (this.state.borrado === 1) {
            return (<><Spinner color="primary" /> </>)
        }


        return (
            <>
                <Row>
                    <div>
                        <div className="row mt-5" >
                            <div className="col-5">
                                <img src={this.state.coverUrl} alt={this.state.name} className="img-thumbnail animate__animated animate__fadeInLeft" />
                            </div>
                            <div className="col 8 animate__animated animate__fadeIn">
                                <h3>Nombre: {this.state.name}</h3>
                                <h4>Descripción: {this.state.description}</h4>
                            </div>
                        </div>
                        <div className="row mt-5 animate__animated animate__fadeIn" >
                            <div className="col-sm-5">
                                <h4>Club:</h4>
                                <h4> </h4>
                            </div>
                            <div className="col-sm-7 col-2">
                                <h4>{this.state.clubId}</h4>
                                <h4> </h4>
                                <br />
                                <Link to={"/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/promociones/EditarPromociones/" + this.state.id}><Button className="btn btn-info">EDITAR</Button></Link>
                                <Button className="mr-1" onClick={() => this.borrado(this.state.id)} color="danger">ELIMINAR</Button>
                            </div>
                        </div>
                    </div>
                </Row>

            </>
        )
    }
}