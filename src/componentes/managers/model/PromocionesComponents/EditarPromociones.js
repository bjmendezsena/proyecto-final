import React from 'react';
import Promociones from '../../../../modelos/Promociones';
import { Link, Redirect } from 'react-router-dom';
import { Input, Row, Col, Label, Form, Spinner } from 'reactstrap';
import '../../../admins/estilos.css'
import promo from '../../../../Images/promo.jpg'

export default class EditarPromociones extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            promotionid: "",
            clubId: "",
            name: "",
            description: "",
            coverUrl: "",
            modificado: 0,
            imgNotFound: promo

        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.guardaPromocion = this.guardaPromocion.bind(this);
        this.cargaDatos = this.cargaDatos.bind(this);
        this.handleChangeImage = this.handleChangeImage.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Promociones.getPromocionById(this.props.match.params.id * 1)
            .then(promociones => {
                this.setState({
                    clubId: promociones.data.clubId,
                    name: promociones.data.name,
                    description: promociones.data.description,
                    coverUrl: promociones.data.coverUrl
                })
            })
            .catch(err => console.log(err));
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleChangeImage(evt) {
        console.log("Uploading");
        var reader = new FileReader();
        var file = evt.target.files[0];
        var self = this;

        reader.onload = function (upload) {
            self.setState({
                coverUrl: upload.target.result
            }, function () {
                console.log(self.state.coverUrl);
            });
        };
        reader.readAsDataURL(file);
    }

    guardaPromocion(e) {
        e.preventDefault();
        let idEdit = this.props.match.params.id * 1;
        let nuevoName = this.state.name;
        let nuevoDescription = this.state.description;
        let nuevoCoverUrl = this.state.coverUrl;
        if (window.confirm("Desea actualizar la información?")) {
            this.setState({ modificado: 1 }) //al ejecutrarse el Formulario en el render ejecutra la función guardaProducto() que pone modificado en true y redirecciona la página a productos una vez guardados los cambios
            Promociones.editPromocion(idEdit, this.state.clubId, nuevoName,
                nuevoDescription, nuevoCoverUrl)
                .then(response => {
                    console.log(response);
                    this.setState({ modificado: 2 })
                })
        }
    }

    render() {

        console.log("M", this.state.modificado);
        if (this.state.modificado === 2) {
            return (
                <Redirect to={"../"} />
            )
        } else if (this.state.modificado === 1) {
            return (<><Spinner color="primary" /> </>)
        }
        if (this.state.coverUrl == null) {
            this.setState({ coverUrl: this.state.imgNotFound })
        }

        return (
            <>
                <Row>
                    <Col>
                        <h1 className="form-group text-center">Editando Promocion: </h1>
                        <br />
                    </Col>
                </Row>
                <Row>
                    <Form onSubmit={this.guardaPromocion} className="formularioPromociones">
                        <Col>
                            <Label htmlFor="clubId">Club:</Label>
                            <Input value={this.state.clubId} onChange={this.handleInputChange} type="text" name="clubId" disabled />
                            <br />
                            <Label htmlFor="name">Nombre:</Label>
                            <Input value={this.state.name} onChange={this.handleInputChange} type="text" name="name" />
                            <br />
                            <Label htmlFor="description">Descripción</Label>
                            <Input value={this.state.description} onChange={this.handleInputChange} type="textarea" name="description" />
                            <br />
                            <Label htmlFor="coverUrl">Cover</Label>
                            <br />
                            <img className="imgForm" src={this.state.coverUrl} alt={this.state.name} />
                            <br />
                            <Label for="coverUrl">Modificar imagen:</Label>
                            <br />
                            <Input onChange={this.handleChangeImage} type="file" name="coverUrl" />
                            <br />
                            <input className="btn btn-success" type='submit' value="Guardar" />
                            <br /><br />
                            <Link to="../" className="link">Volver al listado</Link>
                        </Col>
                    </Form>
                </Row>
            </>
        )
    }
}