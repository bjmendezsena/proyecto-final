import React, { Component } from 'react';
import Promociones from '../../../../modelos/Promociones';
import { Link } from 'react-router-dom';
import { Button, Spinner } from 'reactstrap';

export default class ListadoPromociones extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            idManager: props.match.params.IdManagerCDetalles,
            idClub: props.match.params.IdClubDetalles,
            promociones: [],
            sensor: false
        }
    }
    cargaDatos() {
        Promociones.getPromocionByClub(this.state.idClub * 1)
            .then(data => {
                this.setState({ promociones: data })
                this.setState({ sensor: true });
            })
            .catch(err => console.log(err));
    }

    componentDidMount() {
        console.log("en didMount")
        this.cargaDatos();
    }

    render() {
        if (!this.state.sensor) return <> <h1>Cargando..</h1> <Spinner color="primary" /> </>

        let cardsPromociones = this.state.promociones.data.map(el =>
            <div className="col-sm-8 col-lg-6" key={el.id}>
                <div className="foto">
                    <Link to={"/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/promociones/InfoPromociones/" + el.id}>
                        <div className="col-5">
                            <img className="promo" src={el.coverUrl} alt={el.name} />
                            <div className="link">
                                <h5>Promoción: {el.name}</h5>
                            </div>
                        </div>
                    </Link>
                </div>
            </div>
        );

        return (
            <>
                <br />
                <h1 className="form-group text-center">Listado de promociones{"  "}<Link to={"/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/promociones/AltaPromociones"} className="link"><Button color="success">AÑADIR PROMOCIÓN</Button></Link></h1>
                <br />
                <div className="row">
                    {cardsPromociones}
                </div>
            </>
        )
    }
}
