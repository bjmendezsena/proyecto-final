import React, { Component } from 'react';
import Pedidos from '../../../../modelos/Pedidos';
import { Link } from 'react-router-dom';
import imgPedido from '../../../../Images/pedido.jpg';
import { Spinner } from 'reactstrap';

export default class ListadoProducto extends Component {
    constructor(props) {
        super(props);
        /*console.log("PROPS DESDE PEDIDOS")
        console.log(props);*/
        this.state = {
            sensor: false,
            idManager: props.match.params.IdManagerCDetalles,
            idClub: props.match.params.IdClubDetalles,
            pedidos: []
        }
        console.log(this.state);
    }

    cargaDatos() {
        Pedidos.getPedidoByClub(this.state.idClub * 1)
            .then(data => {
                this.setState({ pedidos: data })
                this.setState({ sensor: true });
            })
            .catch(err => console.log(err));
    }

    componentDidMount() {
        //console.log("en didMount");
        this.cargaDatos();
    }

    render() {
        if (!this.state.sensor) return <> <h1>Cargando..</h1> <Spinner color="primary" /> </>


        let cardsPedidos = this.state.pedidos.data.map(el =>
            <div className="col-sm-8 col-lg-6" key={el.orderId}>
                <div className="foto">
                    <Link to={"/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/pedidos/InfoPedidos/" + el.orderId}>
                        <img className="foto" src={imgPedido} alt={el.orderId} />
                        <div className="link">
                            <h5>Pedido: {el.orderId}</h5>
                        </div>
                    </Link>
                </div>
            </div>
        );
        return (
            <>
                <br />
                <h1 className="form-group text-center">Listado de pedidos</h1>
                <br />
                <div className="row">
                    {cardsPedidos}
                </div>
            </>

        )
    }

}

