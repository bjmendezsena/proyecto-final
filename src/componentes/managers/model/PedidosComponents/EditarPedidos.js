import React from 'react';
import Pedidos from '../../../../modelos/Pedidos';
import { Link, Redirect } from 'react-router-dom';
import { Input, Row, Col, Label, Form, Spinner } from 'reactstrap';
import '../../../admins/estilos.css'

export default class EditarPedidos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orderId: "",
            clubId: "",
            userId: "",
            productId: "",
            promotionId: "",
            price: "",
            status: "",
            modificado: 0
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.guardaPedido = this.guardaPedido.bind(this);
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaDatos();

    }

    cargaDatos() {
        Pedidos.getPedidoById(this.props.match.params.id * 1)
            .then(pedidos => {
                this.setState({
                    clubId: pedidos.data.clubId,
                    userId: pedidos.data.userId,
                    productId: pedidos.data.productId,
                    promotionId: pedidos.data.promotionId,
                    price: pedidos.data.price,
                    status: pedidos.data.status,
                });
            })
            .catch(err => console.log(err));
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    guardaPedido(e) {
        e.preventDefault();
        let idEdit = this.props.match.params.id * 1;
        let nuevoStatus = this.state.status;
        if (window.confirm("Desea actualizar la información?")) {
            this.setState({ modificado: 1 });//al ejecutrarse el Formulario en el render ejecutra la función guardaProducto() que pone modificado en true y redirecciona la página a productos una vez guardados los cambios
            Pedidos.editPedido(idEdit, this.state.clubId, this.state.userId, this.state.productId, this.state.promotionId, this.state.price, nuevoStatus)
                .then(response => {
                    console.log(response);
                    this.setState({ modificado: 2 })
                })
        }
    }

    render() {
        console.log("M", this.state.modificado);
        if (this.state.modificado === 2) {
            return (
                <Redirect to={"../"} />
            )
        } else if (this.state.modificado === 1) {
            return (<><Spinner color="primary" /> </>)
        }
        console.log(this.state);
        return (
            <>
                <Row>
                    <Col>
                        <h1 className="form-group text-center">Editando Pedido: </h1>
                        <br />
                    </Col>
                </Row>
                <Row>
                    <Form onSubmit={this.guardaPedido} className="formulario">
                        <Col >
                            <Label for="clubId">Club:</Label>
                            <Input value={this.state.clubId} onChange={this.handleInputChange} type="text" name="clubId" disabled />
                            <br />

                            <Label for="name">Usuario:</Label>
                            <Input value={this.state.userId} onChange={this.handleInputChange} type="text" name="userId" disabled />
                            <br />

                            <Label for="category">Productos</Label>
                            <Input value={this.state.productId} onChange={this.handleInputChange} type="text" name="productId" disabled />
                            <br />
                            <Label for="price">Promoción</Label>
                            <Input value={this.state.promotionId} onChange={this.handleInputChange} type="number" name="promotionId" disabled />
                            <br />
                            <Label for="price">Precio</Label>
                            <Input value={this.state.price} onChange={this.handleInputChange} type="number" name="price" disabled />
                            <br />
                            <Label for="status">Estado</Label>
                            <Input value={this.state.status} onChange={this.handleInputChange} type="select" name="status">
                                <option value="" disabled hidden>Estado</option>
                                <option value="Pagado">Pagado</option>
                                <option value="No pagado">No pagado</option>
                            </Input>
                            <br />
                            <input className="btn btn-success" type='submit' value="Guardar" />
                            <br />
                            <br />
                            <Link to={"../"} className="link">Volver al listado</Link>
                        </Col>
                    </Form>
                </Row>
            </>
        )
    }
}