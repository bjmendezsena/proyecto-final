import React from 'react';
import Pedidos from '../../../../modelos/Pedidos';
import { Button, Row, Spinner } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';
import imgPedido from '../../../../Images/pedido.jpg';

export default class InfoPedidos extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            idManager: props.match.params.IdManagerCDetalles,
            idClub: props.match.params.IdClubDetalles,
            orderId: "",
            clubId: "",
            userId: "",
            productId: "",
            promotionId: "",
            price: "",
            status: "",
            borrado: 0
        };
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Pedidos.getPedidoById(this.props.match.params.id * 1)
            .then(pedidos => {
                this.setState({
                    orderId: pedidos.data.orderId,
                    clubId: pedidos.data.clubId,
                    userId: pedidos.data.userId,
                    productId: pedidos.data.productId,
                    promotionId: pedidos.data.promotionId,
                    price: pedidos.data.price,
                    status: pedidos.data.status
                });
            })
            .catch(err => console.log(err));
    }

    borrado(idBorrar) {
        if (window.confirm("Desea borrar el pedido #'" + this.state.orderId + "'?")) {
            this.setState({ borrado: 1 })
            Pedidos.deletePedido(idBorrar)
                .then(response => {
                    console.log(response);
                    this.setState({ borrado: 2 })
                })
        }
    }

    render() {
        if (this.state.borrado === 2) {
            return (
                <Redirect to={"../"} />
            )
        }

        if (this.state.borrado === 1) {
            return (<><Spinner color="primary" /> </>)
        }

        return (
            <>
                <Row>
                    <div>
                        <div className="row mt-5" >
                            <div className="col-5">
                                <img className="img-thumbnail animate__animated animate__fadeInLeft" src={imgPedido} alt={this.state.orderId} />
                            </div>
                            <div className="col 8 animate__animated animate__fadeIn">
                                <h3>Usuario: {this.state.userId}</h3>
                                <h4>Promoción: {this.state.promotionId}</h4>
                            </div>
                        </div>
                        <div className="row mt-5 animate__animated animate__fadeIn" >
                            <div className="col-sm-5">
                                <h4>Producto:</h4>
                                <h4>Precio:</h4>
                                <h4>Estado: </h4>
                            </div>
                            <div className="col-sm-7 col-2">
                                <h4>{this.state.productId}</h4>
                                <h4>{this.state.price}€</h4>
                                <h4>{this.state.status}</h4>
                                <br />
                                <Link to={"/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/pedidos/EditarPedidos/" + this.state.orderId}><Button className="btn btn-info">EDITAR</Button></Link>
                                <Button className="mr-1" onClick={() => this.borrado(this.state.orderId)} color="danger">ELIMINAR</Button>
                            </div>
                        </div>
                    </div>
                </Row>
            </>
        )
    }
}