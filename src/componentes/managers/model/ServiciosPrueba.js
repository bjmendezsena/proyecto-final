const URL2 = 'http://46.101.236.229:8080/api/clubservlet';

export default class ServicioPrueba {
    static async listarClubs() {
        return this.accederApi(URL2);
    }

    static async listarClubPorId(id) {
        const URL_CLUBS_DETALLES = `http://46.101.236.229:8080/api/clubservlet/${id}`;
        console.log("listarClubPorId desde servicios prueba")
        console.log(id);
        return this.accederApi(URL_CLUBS_DETALLES);
    }

    static async listarClubPorManager(id) {
        const URL_CLUBS_MANAGERS = `http://46.101.236.229:8080/api/clubservlet/manager/${id}`;
        return this.accederApi(URL_CLUBS_MANAGERS);
    }

    static async listarUsuarios() {
        const URL_USERS = `http://46.101.236.229:8080/api/userservlet/`;
        return this.accederApi(URL_USERS);
    }

    static async lloginUser(state) {
        let mail = state.email;
        let pass = state.password;
        console.log("DESDE SERVICIOS LOGINUSER")
        console.log(mail)
        console.log(pass)
        const URL_USERS = "http://46.101.236.229:8080/api/loginservlet/login/"+mail+"/"+pass;
        console.log(URL_USERS)
        return this.accederApi(URL_USERS);
    }

    static async accederApi(ruta2) {
        return await fetch(ruta2)
            .then(res => res.json())
            .then(
                results => {
                    console.log("SERVICIOSPRUEBAS")
                    console.log(results)
                    return results.data;
                }
            )
    }
    

}