import React from 'react';
import Reservas from '../../../../modelos/Reservas';
import { Button, Row, Spinner } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';
import imgReserva from '../../../../Images/reserva.png';

export default class InfoReservas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idManager: props.match.params.IdManagerCDetalles,
            idClub: props.match.params.IdClubDetalles,
            reservId: "",
            clubId: "",
            userId: "",
            n_people: "",
            notes: "",
            date: "",
            status: "",
            borrado: 0
        };
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Reservas.getReservaById(this.props.match.params.id * 1)
            .then(reservas => {
                this.setState({
                    reservId: reservas.data.reservId,
                    clubId: reservas.data.clubId,
                    userId: reservas.data.userId,
                    n_people: reservas.data.n_people,
                    notes: reservas.data.notes,
                    date: reservas.data.date,
                    status: reservas.data.status
                });
            })
            .catch(err => console.log(err));
    }

    borrado(idBorrar) {
        if (window.confirm("Desea borrar '" + this.state.reservId + "'?")) {
            this.setState({ borrado: 1 })
            Reservas.deleteReserva(idBorrar)
                .then(response => {
                    console.log(response);
                    this.setState({ borrado: 2 })
                })
        }
    }

    render() {

        if (this.state.borrado === 2) {
            return (
                <Redirect to={"../"} />
            )
        }

        if (this.state.borrado === 1) {
            return (<><Spinner color="primary" /> </>)
        }

        //console.log(elementos);

        return (
            <>
                <Row>
                    <div>
                        <div className="row mt-5" >
                            <div className="col-5">
                                <img className="img-thumbnail animate__animated animate__fadeInLeft" src={imgReserva} alt={this.state.orderId} />
                            </div>
                            <div className="col 8 animate__animated animate__fadeIn">
                                <h3>Club: {this.state.clubId}</h3>
                                <h4>Usuario: {this.state.userId}</h4>
                            </div>
                            <div className="row mt-5 animate__animated animate__fadeIn" >
                                <div className="col-sm-5">
                                    <h4>Nº personas:</h4>
                                    <h4>Notas</h4>
                                    <h4>Fecha: </h4>
                                    <h4>Estado: </h4>
                                </div>
                                <div className="col-sm-7 col-2">
                                    <h4>{this.state.n_people}</h4>
                                    <h4>{this.state.notes}</h4>
                                    <h4>{this.state.date}</h4>
                                    <h4>{this.state.status}</h4>
                                    <Link to={"/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/reservas/EditarReservas/" + this.state.reservId}><Button className="btn btn-info">EDITAR</Button></Link>
                                    <Button className="mr-1" onClick={() => this.borrado(this.state.reservId)} color="danger">ELIMINAR</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </Row>
            </>
        )
    }
}