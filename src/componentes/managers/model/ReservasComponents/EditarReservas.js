import React from 'react'; 
import Reservas from '../../../../modelos/Reservas';
import { Link, Redirect } from 'react-router-dom';
import { Input, Row, Col, Label, Form, Spinner } from 'reactstrap';

export default class EditarReservas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            reservId: "",
            clubId: "",
            userId: "",
            n_people: "",
            notes: "",
            date: "",
            status: "",
            modificado: 0
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.guadaReserva = this.guadaReserva.bind(this);
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Reservas.getReservaById(this.props.match.params.id * 1)
            .then(reserva => {
                this.setState({
                    clubId: reserva.data.clubId,
                    userId: reserva.data.userId,
                    n_people: reserva.data.n_people,
                    notes: reserva.data.notes,
                    date: reserva.data.date,
                    status: reserva.data.status
                });
            })
            .catch(err => console.log(err));
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    guadaReserva(e) {
        e.preventDefault();
        let idEdit = this.props.match.params.id * 1;
        let nuevoState = this.state.status;
        if (window.confirm("Desea actualizar la información?")) {
            this.setState({ modificado: 1 })  //al ejecutrarse el Formulario en el render ejecutra la función guardaClub() que pone modificado en true y redirecciona la página a Clubs una vez guardados los cambios
            Reservas.editReserva(idEdit, this.state.clubId, this.state.userId,
                this.state.n_people, this.state.notes, this.state.date, nuevoState)
                .then(response => {
                    console.log(response);
                    this.setState({ modificado: 2 })
                })

        }
    }

    render() {
        console.log("M", this.state.modificado);
        if (this.state.modificado === 2) {
            return (
                <Redirect to={"../"} />
            )
        } else if (this.state.modificado === 1) {
            return (<><Spinner color="primary" /> </>)
        }
        return (
            <>
                <Row>
                    <Col>
                        <h2>Editando Reserva:</h2>
                        <br />
                    </Col>
                </Row>
                <Row>
                    <Form onSubmit={this.guadaReserva} className="formularioReserva">
                        <Col >
                            <Label htmlFor="clubId">Club</Label>
                            <Input type="number" name="clubId" value={this.state.clubId} onChange={this.handleInputChange} disabled />
                            <br />
                            <Label htmlFor="userid">usuario</Label>
                            <Input type="number" name="userid" value={this.state.userId} onChange={this.handleInputChange} disabled />
                            <br />
                            <Label htmlFor="n_people">Número de personas</Label>
                            <Input type="number" name="n_people" value={this.state.n_people} onChange={this.handleInputChange} disabled />
                            <br />
                            <Label htmlFor="notes">Notas</Label>
                            <Input type="text" name="notes" value={this.state.notes} onChange={this.handleInputChange} disabled />
                            <br />
                            <Label htmlFor="date">Fecha</Label>
                            <Input type="date" name="date" value={this.state.date} onChange={this.handleInputChange} disabled />
                            <br />
                            <Label htmlFor="state">Estado</Label>
                            <Input value={this.state.status} onChange={this.handleInputChange} type="select" name="status">
                                <option value="" disabled hidden>Estado</option>
                                <option value="Aceptada">Aceptada</option>
                                <option value="En espera">En espera</option>
                                <option value="Cancelada">Cancelada</option>
                            </Input>
                            <br />
                            <input className="btn btn-success" type='submit' value="Guardar" />
                            <br />
                            <Link to="../" className="link">Volver al listado</Link>
                        </Col>
                    </Form>
                </Row >
            </>
        )
    }

}