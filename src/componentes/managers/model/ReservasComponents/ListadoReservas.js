import React, { Component } from 'react';
import Reservas from '../../../../modelos/Reservas';
import { Link } from 'react-router-dom';
import imgReserva from '../../../../Images/reserva.png';
import { Spinner } from 'reactstrap';

export default class ListadoReservas extends Component {
    constructor(props) {
        super(props);
        console.log("PROPS DESDE RESERVAS")
        console.log(props);
        this.state = {
            idManager: props.match.params.IdManagerCDetalles,
            idClub: props.match.params.IdClubDetalles,
            sensor: false,
            reservas: []
        }
    }
    cargaDatos() {
        Reservas.getReservaByClub(this.state.idClub * 1)
            .then(data => {
                this.setState({ reservas: data })
                this.setState({ sensor: true });
            })
            .catch(err => console.log(err));
    }

    componentDidMount() {
        console.log("en didMount")
        this.cargaDatos();
    }

    render() {
        if (!this.state.sensor) return <> <h1>Cargando..</h1> <Spinner color="primary" /> </>

        let cardsReservas = this.state.reservas.data.map(el =>
            <div className="col-sm-8 col-lg-6" key={el.reservId}>
                <div className="foto">
                    <Link to={"/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/reservas/InfoReservas/" + el.reservId}>
                        <img className="foto" src={imgReserva} alt={el.reservId} />
                        <div className="link">
                            <h5>Reserva: {el.reservId}</h5>
                        </div>
                    </Link>
                </div>
            </div>
        );

        return (
            <>
                <br />
                <h1 className="form-group text-center">Listado de reservas</h1>

                <div className="row">
                    {cardsReservas}
                </div>
            </>
        )
    }

}
