import React from 'react';
import Productos from '../../../../modelos/Productos';
import { Link, Redirect } from 'react-router-dom';
import { Input, Row, Col, Label, Form, Spinner } from 'reactstrap';
import '../../../admins/estilos.css'

export default class EditarProductos extends React.Component {

    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            idClub: props.match.params.IdClubDetalles,
            id: "",
            clubId: "",
            name: "",
            category: "",
            description: "",
            price: "",
            imageUrl: "",
            status: "",
            modificado: 0
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.guardaProducto = this.guardaProducto.bind(this);
        this.cargaDatos = this.cargaDatos.bind(this);
        this.handleChangeImage = this.handleChangeImage.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Productos.getProductoById(this.props.match.params.id * 1)
            .then(producto => {
                this.setState({
                    clubId: producto.data.clubId,
                    name: producto.data.name,
                    category: producto.data.category,
                    description: producto.data.description,
                    price: producto.data.price,
                    imageUrl: producto.data.imageUrl,
                    status: producto.data.status
                });
            })
            .catch(err => console.log(err));
        console.log(this.state.clubId);
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleChangeImage(evt) {
        console.log("Uploading");
        var reader = new FileReader();
        var file = evt.target.files[0];
        var self = this;

        reader.onload = function (upload) {
            self.setState({
                imageUrl: upload.target.result
            }, function () {
                console.log(self.state.imageUrl);
            });
        };
        reader.readAsDataURL(file);
    }

    guardaProducto(e) {
        e.preventDefault();
        let idEdit = this.props.match.params.id * 1;
        let nuevoName = this.state.name;
        let nuevoCategory = this.state.category;
        let nuevoDescription = this.state.description;
        let nuevoPrice = this.state.price;
        let nuevoCoverUrl = this.state.imageUrl;
        let nuevoStatus = this.state.status;
        if (window.confirm("Desea actualizar la información?")) {
            this.setState({ modificado: 1 }) //al ejecutrarse el Formulario en el render ejecutra la función guardaProducto() que pone modificado en true y redirecciona la página a productos una vez guardados los cambios
            Productos.editProducto(idEdit, this.state.idClub, nuevoName,
                nuevoCategory, nuevoDescription, nuevoPrice, nuevoCoverUrl, nuevoStatus)
                .then(response => {
                    console.log(response);
                    this.setState({ modificado: 2 })
                })
        }
    }

    render() {
        console.log("M", this.state.modificado);
        if (this.state.modificado === 2) {
            return (
                <Redirect to={"../"} />
            )
        } else if (this.state.modificado === 1) {
            return (<><Spinner color="primary" /> </>)
        }
        if (this.state.imageUrl == null) {
            this.setState({ coverUrl: this.state.imgNotFound })
        }

        if (this.state)
            return (
                <>
                    <Row>
                        <Col>
                            <h1 className="form-group text-center">Editando producto:</h1>
                            <br />
                        </Col>
                    </Row>
                    <Row>
                        <Form onSubmit={this.guardaProducto} className="formulario">
                            <Col >
                                <Label for="clubId">Club:</Label>
                                <Input value={this.state.idClub} onChange={this.handleInputChange} type="text" name="clubId" disabled />
                                <br />
                                <Label for="name">Nombre:</Label>
                                <Input value={this.state.name} onChange={this.handleInputChange} type="text" name="text" />
                                <br />
                                <Label for="category">Categoria</Label>
                                <Input value={this.state.category} onChange={this.handleInputChange} type="text" name="category" />
                                <br />
                                <Label for="description">Descripción</Label>
                                <Input value={this.state.description} onChange={this.handleInputChange} type="textarea" name="description" />
                                <br />
                                <Label for="price">Precio</Label>
                                <Input value={this.state.price} onChange={this.handleInputChange} type="number" name="price" />
                                <br />
                                <Label for="imageUrl">Imagen</Label>
                                <br />
                                <img className="imgForm" src={this.state.imageUrl} alt={this.state.name} />
                                <br />
                                <Input onChange={this.handleChangeImage} type="file" name="coverUrl" />
                                <br />
                                <Label for="status">Estado</Label>
                                <Input value={this.state.status} onChange={this.handleInputChange} type="select" name="status">
                                    <option value="" disabled hidden>Estado</option>
                                    <option value="true">Disponible</option>
                                    <option value="false">No disponible</option>
                                </Input>
                                <br />
                                <input className="btn btn-success" type='submit' value="Guardar" />
                                <br /><br />
                                <Link to="../" className="link">Volver al listado</Link>
                            </Col>
                        </Form>
                    </Row>
                </>
            )
    }
}