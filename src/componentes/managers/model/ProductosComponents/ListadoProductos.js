import React, { Component } from 'react';
import Productos from '../../../../modelos/Productos';
import { Button, Spinner } from 'reactstrap';
import { Link } from 'react-router-dom';

export default class ListadoProductos extends Component {
    constructor(props) {
        super(props);
        /*console.log("PROPS DESDE PRODUCTOS")
        console.log(props);*/
        this.state = {
            sensor: false,
            idManager: props.match.params.IdManagerCDetalles,
            idClub: props.match.params.IdClubDetalles,
            productos: []
        }
        console.log(this.state);
    }

    cargaDatos() {
        Productos.getProductoByClub(this.state.idClub * 1)
            .then(data => {
                this.setState({ productos: data })
                this.setState({ sensor: true });
            })
            .catch(err => console.log(err));
    }

    componentDidMount() {
        //console.log("en didMount")
        this.cargaDatos();
    }
    render() {
        if (!this.state.sensor) return <> <h1>Cargando..</h1> <Spinner color="primary" /> </>

        let cardsProductos = this.state.productos.data.map(el =>
            <div className="col-sm-8 col-lg-6" key={el.id}>
                <div className="foto">
                    <Link to={"/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/productos/InfoProductos/" + el.id}>
                        <img className="foto" src={el.imageUrl} alt={el.name} />
                        <div className="link">
                            <h5>Producto: {el.name}</h5>
                        </div>

                        {/*console.log(el)*/}
                        <h1>{el.name}</h1>
                        <h6>Precio: {el.price} €</h6>
                        <h6>Estado: {el.status.toString()}</h6>
                    </Link>
                </div>
            </div>
        );
        return (
            <>
                <br />
                <h1 className="form-group text-center">Listado de productos{"  "}<Link to={"/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/productos/AltaProductos/" + this.state.id} className="link"><Button color="success">AÑADIR PRODUCTO</Button></Link></h1>
                <br />
                <div className="row">
                    {cardsProductos}
                </div>
            </>
        )
    }
}

