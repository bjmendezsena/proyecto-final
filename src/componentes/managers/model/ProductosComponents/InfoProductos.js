import React from 'react';
import Productos from '../../../../modelos/Productos';
import { Button, Row, Spinner } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';

export default class InfoProductos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idManager: props.match.params.IdManagerCDetalles,
            idClub: props.match.params.IdClubDetalles,
            id: "",
            clubId: "",
            name: "",
            category: "",
            description: "",
            price: "",
            imageUrl: "",
            status: "",
            estado: "",
            borrado: 0
        };
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Productos.getProductoById(this.props.match.params.id * 1)
            .then(producto => {
                this.setState({
                    id: producto.data.id,
                    clubId: producto.data.clubId,
                    name: producto.data.name,
                    category: producto.data.category,
                    description: producto.data.description,
                    price: producto.data.price,
                    imageUrl: producto.data.imageUrl,
                    status: producto.data.status
                });
            })
            .catch(err => console.log(err));
    }

    borrado(idBorrar) {
        if (window.confirm("Desea borrar '" + this.state.name + "'?")) {
            this.setState({ borrado: 1 })
            Productos.deleteProducto(idBorrar)
                .then(response => {
                    console.log(response);
                    this.setState({ borrado: 2 })
                })
        }
    }

    render() {
        if (this.state.borrado === 2) {
            return (
                <Redirect to={"../"} />
            )
        }

        if (this.state.borrado === 1) {
            return (<><Spinner color="primary" /> </>)
        }

        if (this.state.status === true) {
            var estado = "Disponible";
        } else {
            estado = "No disponible";
        }

        return (
            <>
                <Row>
                    <div>
                        <div className="row mt-5" >
                            <div className="col-5">
                                <img src={this.state.imageUrl} alt={this.state.name} className="img-thumbnail animate__animated animate__fadeInLeft" />
                            </div>
                            <div className="col 8 animate__animated animate__fadeIn">
                                <h3> {this.state.name}</h3>
                                <h4 >{this.state.description}</h4>
                            </div>
                        </div>
                        <div className="row mt-5 animate__animated animate__fadeIn" >
                            <div className="col-sm-5">
                                <h4>Club:</h4>
                                <h4>Categoria: </h4>
                                <h4>Precio</h4>
                                <h4>Estado: </h4>
                            </div>
                            <div className="col-sm-7 col-2">
                                <h4>{this.state.idClub}</h4>
                                <h4>{this.state.category}</h4>
                                <h4>{this.state.price}€</h4>
                                <h4>{estado}</h4>
                                <br />
                                <Link to={"/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/productos/EditarProductos/" + this.state.id}><Button className="btn btn-info">EDITAR</Button></Link>
                                {/*<Link to={"/EditarProductos/" + this.state.id}><Button className="btn btn-info">EDITAR</Button></Link>*/}
                                <Button className="mr-1" onClick={() => this.borrado(this.state.id)} color="danger">ELIMINAR</Button>
                            </div>
                        </div>
                    </div>
                </Row>
            </>
        )
    }
}