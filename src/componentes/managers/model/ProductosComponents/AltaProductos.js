import React, { Component } from 'react';
import Productos from '../../../../modelos/Productos';
import { Input, Label, Form, Spinner } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";

export default class AltaProductos extends Component {

    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            productos: Productos.getProducto(),
            idClub: props.match.params.IdClubDetalles,
            clubId: '',
            name: '',
            category: '',
            description: '',
            price: '',
            imageUrl: '',
            status: '',
            altaProducto: 0
        }
        this.productoNuevo = this.productoNuevo.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleChangeImage = this.handleChangeImage.bind(this);
        console.log(this.state);
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }
    
    handleChangeImage(evt) {
        console.log("Uploading");
        var reader = new FileReader();
        var file = evt.target.files[0];
        var self = this;

        reader.onload = function (upload) {
            self.setState({
                imageUrl: upload.target.result
            });
        };
        reader.readAsDataURL(file);
    }

    productoNuevo(e) {
        e.preventDefault();

        if (this.state.name !== "") {
            this.setState({ altaProducto: 1 });
            Productos.addProducto(
                this.state.idClub,
                this.state.name,
                this.state.category,
                this.state.description,
                this.state.price,
                this.state.imageUrl,
                this.state.status
            )
                .then(response => {
                    console.log(response);
                    this.setState({ altaProducto: 2 });
                })
        } else {
            alert("No se puede dar de alta este club");
        }
    }

    render() {

        const { name, category, description, price, status } = this.state;
        if (this.state.altaProducto === 2) {
            return (
                <Redirect to={"../"} />
            )
        } else if (this.state.altaProducto === 1) {
            return (<><Spinner color="primary" /> </>)

        }

        return (
            <>
                <h1 className="form-group text-center">Añadir producto</h1>
                <div className="row">
                    <Form className="formulario" onSubmit={this.productoNuevo} method="POST">
                        <br />
                        <Label htmlFor="clubid"><b>Club:</b></Label>
                        <Input type="number" name="clubId" placeholder="Club" onChange={this.handleInputChange} value={this.state.idClub} disabled />
                        <br />
                        <Label htmlFor="name"><b>Nombre:</b></Label>
                        <Input type="text" name="name" placeholder="Nombre" onChange={this.handleInputChange} value={name} />
                        <br />
                        <Label htmlFor="category"><b>Categoria:</b></Label>
                        <Input type="text" name="category" min="0" placeholder="Categoria" onChange={this.handleInputChange} value={category} />
                        <br />
                        <Label htmlFor="description"><b>Descripción:</b></Label>
                        <Input type="textarea" name="description" min="0" placeholder="Descipción" onChange={this.handleInputChange} value={description} />
                        <br />
                        <Label htmlFor="price"><b>Precio:</b></Label>
                        <Input type="number" name="price" placeholder="Precio" onChange={this.handleInputChange} value={price} />
                        <br />
                        <Label htmlFor="imageUrl"><b>Imagen:</b></Label>
                        <Input type="file" name="imageUrl" placeholder="Imagen" onChange={this.handleChangeImage} encType="multipart/form-data" />
                        <br /><br />
                        <Label htmlFor="status"><b>Estado:</b></Label>
                        <Input value={status} onChange={this.handleInputChange} type="select" name="status" placeholder="Estado" required>
                            <option value="" disabled hidden>Estado</option>
                            <option value="true">Disponible</option>
                            <option value="false">No disponible</option>
                        </Input>
                        <br />
                        <Input type="submit" value="Dar de alta" />
                        <br />
                        <Link to="/Productos" className="link">Volver al listado</Link>
                    </Form>
                </div>

            </>
            //<i className="fa fa-plus" aria-hidden="true"></i> en vez de Añadir linea 134
        )
    }
}
