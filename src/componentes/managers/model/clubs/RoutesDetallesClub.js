import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

export const RoutesDetallesClub = () => {
    return (
        <>
            <div className = "container mt-2">

                <Switch>
                   
{/**----------------------------------------------------------------------------------*/}
                    <Route exact path = "/principalmanagers/club/:idClub" component = {DetallesClub}/>
                    
{/**-----------------------------------------------------------------------------------*/}
                </Switch>
            </div>
        </>
    )
}