import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import ListadoPedidos from '../PedidosComponents/ListadoPedidos';

import EditarPedidos from '../PedidosComponents/EditarPedidos';
import InfoPedidos from '../PedidosComponents/InfoPedidos';
import AltaProductos from '../ProductosComponents/AltaProductos';
import EditarProductos from '../ProductosComponents/EditarProductos';
import InfoProductos from '../ProductosComponents/InfoProductos';

import ListadoProductos from '../ProductosComponents/ListadoProductos';
import AltaPromociones from '../PromocionesComponents/AltaPromociones';
import EditarPromociones from '../PromocionesComponents/EditarPromociones';
import InfoPromociones from '../PromocionesComponents/InfoPromociones';

import ListadoPromociones from '../PromocionesComponents/ListadoPromociones';
import EditarReservas from '../ReservasComponents/EditarReservas';
import InfoReservas from '../ReservasComponents/InfoReservas';
import ListadoReservas from '../ReservasComponents/ListadoReservas';



import ServicioPrueba from '../ServiciosPrueba';
import MenuClub from './MenuClub';

import './MenuStyle.scss'


export default class DetallesClub extends Component {
    constructor(props) {
        super(props);

        const idClub = this.props.match.params.idClub;

        this.state = {
            club: null
        };

        ServicioPrueba.listarClubPorId(idClub)
            .then(resp => {

                this.setState({ club: resp });
            });
    }



    render() {

        const club = this.state.club;
        if (club === null) return "";

        return (

            <div>
                <div>
                    <div className="row mt-5 container" >
                        <div className="col 4">
                            <img className="img-thumbnail animate__animated animate__fadeInLeft"
                                src={club.coverUrl}
                                alt={club.name}
                            />
                        </div>
                        <div className="col 8 animate__animated animate__fadeIn">
                            <h3> {club.name}</h3>
                            <h6 >{club.description}</h6>

                        </div>
                    </div>

                    <div className="row mt-5 animate__animated animate__fadeIn" >

                        <div className="col 4">
                            <MenuClub propsDetalles={this.props} />
                        </div>
                        <div >
                            <Switch>
                                {/*principalmanagers/:idManager/club/:idClub*/}

                                {/**----------------------------------------------------------------------------------*/}
                                <Route exact path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/pedidos" component={ListadoPedidos} />
                                <Route exact path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/reservas" component={ListadoReservas} />
                                <Route exact path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/promociones" component={ListadoPromociones} />
                                <Route exact path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/productos" component={ListadoProductos} />
                                {/**-----------------------------------------------------------------------------------*/}


                                <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/pedidos/InfoPedidos/:id" component={InfoPedidos} />
                                <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/pedidos/EditarPedidos/:id" component={EditarPedidos} />

                                <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/productos/AltaProductos" component={AltaProductos} />
                                <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/productos/InfoProductos/:id" component={InfoProductos} />
                                <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/productos/EditarProductos/:id" component={EditarProductos} />


                                <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/reservas/InfoReservas/:id" component={InfoReservas} />
                                <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/reservas/EditarReservas/:id" component={EditarReservas} />


                                <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/promociones/AltaPromociones" component={AltaPromociones} />
                                <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/promociones/InfoPromociones/:id" component={InfoPromociones} />
                                <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/promociones/EditarPromociones/:id" component={EditarPromociones} />



                                {/**-----------------------------------------------------------------------------------*/}
                            </Switch>
                        </div>
                    </div>
                    {/* <div >
                        <Switch>
                            {/*principalmanagers/:idManager/club/:idClub*/}

                    {/**----------------------------------------------------------------------------------*/}
                    {/*<Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/pedidos" component={ListadoPedidos} />
                            <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/reservas" component={ListadoReservas} />
                            <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/promociones" component={ListadoPromociones} />
                            <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/productos" component={ListadoProductos} />
                            <Route path="/principalmanagers/:IdManagerCDetalles/club/:IdClubDetalles/productos/info/:id" component={InfoProductos} />
                            {/**-----------------------------------------------------------------------------------*/}
                    {/*</Switch>
                   </div>*/}
                </div>
            </div>



        );
    }
}
