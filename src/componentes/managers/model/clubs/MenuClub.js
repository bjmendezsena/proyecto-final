import React, {Component} from 'react';
import ComponentesMenu from './ComponentesMenu';

export default  class MenuClub extends Component {
    constructor(props) {
        super(props);
        this.state = {
            idClub:this.props.propsDetalles.match.params.idClub,
            idManager:this.props.propsDetalles.match.params.idManager
          };
        
    }
    render() {
        return (
            <aside className='journal_menu'>
                <div className='journal_navbar'>
                    <h3 className="mt-5">
                        <span>
                            Menú
                        </span>
                    </h3>
                </div>
                <ComponentesMenu propsMenuClubs={this.props.propsDetalles}/>
            </aside>
        );
    }
}
