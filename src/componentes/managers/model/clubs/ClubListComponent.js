import React, {Component} from 'react';
import ClubComponent from './ClubComponent';
export default class ClubListComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            idManager: this.props.idManagerClubList
        }
    }
    
    render() {
        const clubs = this.props.clubList;

        if(clubs.length >0 || clubs !== 'Este usuario no tiene discotecas manejando'){
            return (
                <div className="container animate__animated animate__fadeIn">
                <h1 className="text-center display-5 my-4">Lista de discotecas</h1>
                    <div className="card-columns ">
                       
                        {clubs.map(clubElement => (
                            <div >
                                <ClubComponent clubProp={clubElement} idManagerclub = {this.state.idManager} />
                            </div>
                        ))}
                        
                    </div>
                
                </div>
                
            );

        }else{
            return (
                <div>
                    ESTE MANAGER NO TIENE CLUB
                </div>
            );
        }
        
    }
}
