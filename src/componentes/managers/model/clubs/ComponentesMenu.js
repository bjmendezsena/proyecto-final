
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';


export default class ComponentesMenu extends Component {


    constructor(props) {
        super(props);
        this.state = {
            idManager: this.props.propsMenuClubs.match.params.idManager,
            idClub: this.props.propsMenuClubs.match.params.idClub,
            url: ''
        };
    }
    onClickFunction = (e) => {
        e.preventDefault();
        
        const url = this.props.propsMenuClubs.history.location.pathname;
        console.log("DESDE COMPONENTES MENU");
        console.log(this.state)
        console.log(url);
        this.setState({ url: "/principalmanagers/" + this.state.idManager + "/club/" + this.state.idClub + "/" + e.target.id });

        /* switch (e.target.id) {
             case 'productos':
                  //this.setState({url:this.state.idClub+"/productos"}); 
                  history.push(`${this.state.idClub}/productos`);
                  console.log(history)
                 break;
              case 'reservas':
                  history.push(`${this.state.idClub}/reservas`);
                 // history.push(url+'/reservas');
                 break;
              case 'pedidos':
                  history.push(`${this.state.idClub}/pedidos`);
               //   history.push('/pedidos');
                 break;
              case 'promociones':
                  history.push(`${this.state.idClub}/promociones`);
              //    history.push('/promociones');
                //  console.log( history.push)
                  //history.replace('/principalmanagers/club/productos');
                 break;
             default:
                 break;
         }*/
    }
    render() {
        if (this.state.url !== '') {
            let direccion = this.state.url;
            this.setState({ url: '' });
            return <Redirect to={direccion} />

        }

        return (
            <div>
                <div className="journal__elemento__menu" id='productos' onClick={this.onClickFunction}>
                    Productos
                </div>

                <div className="journal__elemento__menu" id='reservas' onClick={this.onClickFunction}>
                    Reservas
                </div>
                <div className="journal__elemento__menu" id='pedidos' onClick={this.onClickFunction}>
                    Pedidos
                </div>
                <div className="journal__elemento__menu" id='promociones' onClick={this.onClickFunction}>
                    Promociones
                </div>

            </div>

        )


    }
}

