import React,{Component} from 'react';
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import './ClubComponent.css';

export default class ClubComponent extends Component {  
    constructor(props){
        super(props);
        this.state = {
            club: this.props.clubProp,
            idManager:this.props.idManagerclub
        }

    }

    render() {
        const club = this.state.club
        console.log(club);
        if(club)
        return (
            ////principalmanagers/:idManager/club/:idClub
            // <h6 >{club.description}</h6> debajo de club.name linea 29
                    <div className="card"  >
                    <Link to={`/principalmanagers/${this.state.idManager}/club/${this.state.club.id}`}>
                         <img  className="card-img-top clubComponentId" 
                                     src={club.coverUrl}
                                       alt={club.name}
                                />
                                </Link>   
                        <div >
                            <h2 className="textAlignCenter"> {club.name}</h2>
                            
                        </div>
                    </div>
                )
    }  
     
}
