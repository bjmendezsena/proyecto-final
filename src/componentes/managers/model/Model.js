
const URL = 'http://localhost:3000/api/clubs';

 class AccesoAPI {
    static async listarDiscos() {
        return this.accederApi(URL);
    }

    static async accederApi(ruta) {
        return await fetch(ruta)
            .then(res => res.json())
            .then(
                results => {
                    console.log(results);
                    return results;
                }
            )
    }
}

module.exports = AccesoAPI;
let prueba = AccesoAPI.listarDiscos.results;
console.log(prueba);
