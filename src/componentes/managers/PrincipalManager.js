
import React,{Component} from 'react';
import './PrincipalManager.css';
import ClubListComponent from './model/clubs/ClubListComponent';
import ServicioPrueba from './model/ServiciosPrueba';



export default  class PrincipalManager extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listClubs:[],
            idManager:this.props.match.params.idManager,
            userManager: this.props.history.location.state
          };
          ServicioPrueba.listarClubPorManager(this.props.match.params.idManager)
          .then(resp =>{
              
              this.setState({listClubs : resp })
          })
    }
    
    render() {

        let clubs = this.state.listClubs;
        
        return (
            <div>
            
                {clubs.length >0 || clubs !== 'Este usuario no tiene discotecas'?
                    <ClubListComponent clubList = {clubs} idManagerClubList = {this.state.userManager.id}/>
                    :""}
            </div>
            
        );
    }
}
