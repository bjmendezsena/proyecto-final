import React from 'react';
import Usuario from '../../../modelos/Usuario';
import { Form, Row, Input, Label, Spinner } from 'reactstrap';
//import ButtonComponent from '../../buttonComponent/ButtonComponent';
import { Redirect, Link } from 'react-router-dom';
import '../estilos.css';

class FormularioAltaUsuario extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            usuarios: Usuario.getUsuarios(),
            userName: '',
            mail: '',
            password: '',
            role: '',
            phone: '',
            birthDate: '',
            altaUsuario: 0
        }
        this.usuarioNuevo = this.usuarioNuevo.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange = ({ target }) => {
        this.setState(
            {
                ...this.state,
                [target.name]: target.value
            }
        );
    }

    usuarioNuevo(e) {
        e.preventDefault();
        if (this.state.userName !== "" && this.state.mail !== "" && this.state.role !== "") {
            this.setState({altaUsuario: 1});
            Usuario.addUsuario(
                this.state.userName,
                this.state.mail,
                this.state.password,
                this.state.role,
                this.state.phone,
                this.state.birthDate) 
                .then(response => {
                    console.log(response);
                    this.setState({altaUsuario: 2});
                })
        } else {
            alert("No se puede dar de alta este usuario")
        }
    }

    render() {
        const { userName, mail, password, role, phone, birthDate } = this.state;
        if (this.state.altaUsuario === 2) {
            return (
                <Redirect to={"../Usuarios"} />
            )
        }else if (this.state.altaUsuario === 1){
            return(<><Spinner color="primary" /> </>) 
        }

        return (
            <>
                <h1 className="form-group text-center">Añadir nuevo usuario</h1>
                <Row>
                    <Form onSubmit={this.usuarioNuevo} className="formulario labelFormulario">
                        <br />
                        <Label><b>Nombre:</b></Label>
                        <Input
                            value={userName}
                            onChange={this.handleInputChange}
                            type="text"
                            name="userName"
                            placeholder="Nombre de usuario"></Input>
                        <br />
                        <Label><b>Correo electrónico:</b></Label>
                        <Input
                            value={mail}
                            onChange={this.handleInputChange}
                            type="email"
                            name="mail"
                            placeholder="Correo electrónico"></Input>
                        <br />
                        <Label><b>Password:</b></Label>
                        <Input
                            value={password}
                            onChange={this.handleInputChange}
                            type="password"
                            name="password"
                            placeholder="Password"></Input>
                        <br />
                        <Label><b>Rol:</b></Label>
                        <Input
                            value={role}
                            onChange={this.handleInputChange}
                            type="select"
                            name="role"
                            placeholder="rol"
                            required>
                            <option value="" disabled hidden>Rol</option>
                            <option value="admin">Admin</option>
                            <option value="manager">Mánager</option>
                        </Input>
                        <br />
                        <Label><b>Número de teléfono:</b></Label>
                        <Input
                            value={phone}
                            onChange={this.handleInputChange}
                            type="tel"
                            name="phone"
                            placeholder="Teléfono"></Input>
                        <br />
                        <Label><b>Fecha de nacimiento:</b></Label>
                        <Input
                            value={birthDate}
                            onChange={this.handleInputChange}
                            type="date"
                            name="birthDate"
                            placeholder="Fecha de nacimiento"></Input>
                        <br />
                        <br />
                        <Input type="submit" value="Dar de alta" />
                        <br />
                        
                        <Link to="/Usuarios">Volver al listado</Link>
                    </Form>
                </Row>
            </>
        )
    }
}
export default FormularioAltaUsuario;
