import React from 'react';
import Usuario from '../../../modelos/Usuario';
import { Link } from 'react-router-dom';
import { Button, Spinner } from 'reactstrap';
import '../estilos.css';
import imgUser from '../../../Images/imgUser.png';

class ListadoUsuarios extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            usuarios: [],
            sensor: false
        }
    }

    cargaDatos() {
        Usuario.getUsuarios()
            .then(data => {
                this.setState({ usuarios: data });
                this.setState({sensor: true})
            })
            .catch(err => console.log(err));
    }

    componentDidMount() {
        this.cargaDatos();
        console.log("en didMount")
    }

    render() {

        if (!this.state.sensor) return <> <h1>Cargando..</h1> <Spinner color="primary" /> </>
        
        let cardsUsuarios = this.state.usuarios.map(el =>
            <div className="col-sm-8 col-lg-6" key={el.id}>
                <Link to={"/InfoUsuario/" + el.id}>
                    <img className="foto" src={imgUser} alt="imgUser" />
                    <h5>{el.userName}</h5>
                    <h5>Tlf: {el.phone} </h5>
                    <h5>Correo: {el.mail}</h5>
                </Link>
            </div>
        );

        return (
            <>
                <br />
                <h1 className="form-group text-center">Listado de usuarios
                {"  "}<Link to="/AltaUsuario" className="link">
                    <br/>
                    <Button color="success">AÑADIR USUARIO</Button>
                    </Link>
                </h1>
                <br />
                <div className="row">
                    {cardsUsuarios}
                </div>
            </>
        )
    }
}
export default ListadoUsuarios;