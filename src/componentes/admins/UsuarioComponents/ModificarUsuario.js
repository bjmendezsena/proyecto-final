import React from 'react';
import Usuario from '../../../modelos/Usuario';
import { Link, Redirect } from 'react-router-dom';
import { Input, Row, Col, Label, Form, Spinner } from 'reactstrap';

export default class ModificarUsuario extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: "",
            userName: "",
            mail: "",
            password: "",
            role: "",
            phone: "",
            birthDate: "",
            modificado: 0,

        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.guardaUsuario = this.guardaUsuario.bind(this);
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Usuario.getUsuarioById(this.props.match.params.id * 1)
            .then(usuario => {
                this.setState({
                    userName: usuario.userName,
                    mail: usuario.mail,
                    password: usuario.password,
                    role: usuario.role,
                    phone: usuario.phone,
                    birthDate: usuario.birthDate
                });
            })
            .catch(err => console.log(err));
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    guardaUsuario(e) {
        e.preventDefault();
        let idModificar = this.props.match.params.id * 1;
        let nuevoNombre = this.state.userName;
        let nuevoCorreo = this.state.mail;
        let nuevoPassword = this.state.password;
        let nuevoTelefono = this.state.phone;
        let nuevoFechaNacimiento = this.state.birthDate;
      
        if (window.confirm("Desea actualizar la información?")) {
            this.setState({modificado:1});
            Usuario.modificaUsuario(idModificar, nuevoNombre, nuevoCorreo, nuevoPassword,
            nuevoTelefono, nuevoFechaNacimiento)
                .then(response => {
                    console.log(response);
                    this.setState({modificado:2})
                }
            )
        }
    }

    render() {
        if (this.state.modificado === 2) {
            return (
                <Redirect to={"../Usuarios"} />
            )
        }else if (this.state.modificado === 1){
            return(<><Spinner color="primary" /> </>)
        }

        return (
            <>
                <Row>
                    <Col className="textAlignCenter">
                        <h2>Editando usuario: </h2>
                        <br />
                    </Col>
                </Row>
                <Row>
                    <Form onSubmit={this.guardaUsuario} className="formulario">
                        <Col >
                            <Label for="userName">Nombre:</Label>
                            <Input value={this.state.userName} onChange={this.handleInputChange} type="text" name="userName" />
                            <br />
                            <Label for="mail">Correo Electrónico:</Label>
                            <Input value={this.state.mail} onChange={this.handleInputChange} type="email" name="mail" />
                            <br />

                            <Label for="password">Password:</Label>
                            <Input value={this.state.password} onChange={this.handleInputChange} type="text" name="password" />
                            <br />

                            <Label for="role">Rol:</Label>
                            <Input disabled value={this.state.role} onChange={this.handleInputChange} type="text" name="role" />
                            <br />

                            <Label for="phone">Teléfono:</Label>
                            <Input value={this.state.phone} onChange={this.handleInputChange} type="number" name="phone" />
                            <br />

                            <Label for="birthDate">Fecha de nacimiento:</Label>
                            <Input value={this.state.birthDate} onChange={this.handleInputChange} type="date" name="birthDate" />
                            <br />

                            <input className="btn btn-success" type='submit' value="Guardar" />
                            <br />
                            <br />
                            <Link to="/Usuarios">Volver al listado</Link>
                        </Col>
                    </Form>
                </Row>
            </>
        )
    }
}