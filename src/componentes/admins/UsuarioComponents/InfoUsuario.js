import React from 'react';
import Usuario from '../../../modelos/Usuario';
import { Col, Row, Container, Button, Spinner } from 'reactstrap';
//import ButtonComponent from '../../buttonComponent/ButtonComponent';
import { Link, Redirect } from 'react-router-dom';
import '../estilos.css';
import imgUser from '../../../Images/imgUser.png';

class InfoUsuario extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            userName: '',
            mail: '',
            password: '',
            role: '',
            phone: '',
            birthDate: '',
            borrado: 0        };
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Usuario.getUsuarioById(this.props.match.params.id * 1)
            .then(usuario => {
                this.setState({
                    id: usuario.id,
                    userName: usuario.userName,
                    mail: usuario.mail,
                    password: usuario.password,
                    role: usuario.role,
                    phone: usuario.phone,
                    birthDate: usuario.birthDate
                });
            })
            .catch(err => console.log(err));
    }

    borrar(idBorrar) {
        if (window.confirm("Desea borrar '" + this.state.userName + "'?")) {
            this.setState({ borrado: 1 })
            Usuario.eliminaUsuario(idBorrar) 
            .then(response => {
                console.log(response);
                this.setState({ borrado: 2 })
            })
        }
    }

    render() {
        if (this.state.borrado === 2) {
            return (
                <Redirect to={"../Usuarios"} />
            )
        }

        if (this.state.borrado === 1){
            return(<><Spinner color="primary" /> </>)
        }

        return (
            <>

<Container>
                    <Row>
                        <Col lx="6" md="6" xs="12">
                        <img className="img-thumbnail animate__animated animate__fadeInLeft imgUser"
                                src={imgUser}
                                alt={this.state.userName}
                            />
                        </Col>
                        <Col lx="6" md="6" xs="12" className="mt-3">
                        <h3> {this.state.userName}</h3>
                        </Col>
                    </Row>
                    <Row>
                        <Col lx="6" md="6" xs="12">
                        </Col>
                        <Col lx="6" md="6" xs="12" className="labelFormularioInfo">

                        <h4>Correo electrónico: {this.state.mail}</h4>
                            <h4>Contraseña: {this.state.password}</h4>
                            <h4>Rol: {this.state.role}</h4>
                            <h4>Teléfono: {this.state.phone}</h4>
                            <h4>Fecha de nacimiento: {this.state.birthDate}</h4>
                        </Col>
                    </Row>
                    <Row className="buttonFormularioInfo">
                        <Col lx="3" md="3" xs="5">
                        </Col>
                        <Col lx="3" md="3" xs="5">
                        </Col>
                        <Col lx="3" md="3" xs="5">
                        <Link to={"/ModificarUsuario/" + this.state.id} ><Button color="primary">EDITAR</Button></Link>
                            </Col>

                        <Col lx="3" md="3" xs="5">
                        <Button className="mr-1" onClick={() => this.borrar(this.state.id)} color="danger">ELIMINAR</Button>
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
}
export default InfoUsuario;