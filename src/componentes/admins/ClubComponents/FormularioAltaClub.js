import React from 'react';
import Club from '../../../modelos/Club';
import { Form, Input, Label, Row, Spinner  } from 'reactstrap';
//import ButtonComponent from '../../buttonComponent/ButtonComponent';
import { Redirect, Link } from 'react-router-dom';
import '../estilos.css';

class FormularioAltaClub extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            clubs: Club.getClubs(),
            clubName: '',
            streetName: '',
            streetNumber: '',
            postalCode: '',
            city: '',
            description: '',
            ambience: '',
            phone: '',
            dressCode: '',
            coverUrl: '',
            latitude: '',
            longitude: '',
            altaClub: 0
        }
        this.clubNuevo = this.clubNuevo.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleChangeImage = this.handleChangeImage.bind(this);
    }

    handleInputChange = ({ target }) => {
        this.setState(
            {
                ...this.state,
                [target.name]: target.value
            }
        );
    }

    handleChangeImage (evt) {
        console.log("Uploading");
        var reader = new FileReader();
        var file = evt.target.files[0];
        var self = this;
    
        reader.onload = function(upload) {
            self.setState({
                coverUrl: upload.target.result
            });
        };
        reader.readAsDataURL(file);
    }

    clubNuevo(e) {
        e.preventDefault();

        if (this.state.clubName !== "") {
            this.setState({altaClub: 1});
            Club.addClub(
                this.state.clubName, 
                this.state.streetName,
                this.state.streetNumber,
                this.state.postalCode,
                this.state.city,
                this.state.description,
                this.state.ambience,
                this.state.phone,
                this.state.dressCode,
                this.state.latitude,
                this.state.longitude,
                this.state.coverUrl
                )
                .then(response => {
                    console.log(response);
                    this.setState({altaClub: 2});
                })
        } else {
            alert("No se puede dar de alta este club");
        }
    }

    render() {
        const { clubName, streetName, streetNumber, postalCode, city, description, ambience, phone, dressCode, latitude, longitude } = this.state;

        if (this.state.altaClub === 2) {
            //this.setState({altaClub: 0})
            return (
                <Redirect to={"../Clubs"} />
            )
        }else if (this.state.altaClub === 1){
            //this.setState({altaClub:0})
            
            return(<><Spinner color="primary" /> </>)
            
        }

        return (
            <>
                <h1 className="form-group text-center">Añadir nuevo club</h1>
                <Row>
                    <Form onSubmit={this.clubNuevo} className="formulario labelFormulario">
                        <br />
                        <Label><b>Nombre del club: </b></Label>
                        <Input
                            type="text"
                            name="clubName"
                            value={clubName}
                            onChange={this.handleInputChange}
                            placeholder="Nombre del club" />
                        <br />
                        <Label><b>Calle: </b></Label>
                        <Input
                            type="text"
                            name="streetName"
                            value={streetName}
                            onChange={this.handleInputChange}
                            placeholder="Calle" />
                        <br />
                        <Label><b>Número: </b></Label>
                        <Input
                            type="number"
                            min="0"
                            name="streetNumber"
                            value={streetNumber}
                            onChange={this.handleInputChange}
                            placeholder="Número" />
                        <br />
                        <Label><b>Código postal: </b></Label>
                        <br />
                        <Input
                            type="text"
                            name="postalCode"
                            value={postalCode}
                            onChange={this.handleInputChange}
                            placeholder="C.P." />
                        <br />
                        <Label><b>Ciudad: </b></Label>
                        <Input
                            type="text"
                            name="city"
                            value={city}
                            onChange={this.handleInputChange}
                            placeholder="Ciudad" />
                        <br />
                        <Label><b>Descripción: </b></Label>
                        <Input
                            type="textArea"
                            name="description"
                            value={description}
                            onChange={this.handleInputChange}
                            placeholder="Descripción" />
                        <br />
                        <Label><b>Ambiente: </b></Label>
                        <Input
                            type="text"
                            name="ambience"
                            value={ambience}
                            onChange={this.handleInputChange}
                            placeholder="Ambiente" />
                        <br />
                        <Label><b>Teléfono: </b></Label>
                        <Input
                            type="tel"
                            name="phone"
                            value={phone}
                            onChange={this.handleInputChange}
                            placeholder="Teléfono" />
                        <br />
                        <Label><b>Código de vestimenta: </b></Label>
                        <Input
                            type="text"
                            name="dressCode"
                            value={dressCode}
                            onChange={this.handleInputChange}
                            placeholder="Código de vestimenta" />
                        <br />
                        <Label><b>Latitud: </b></Label>
                        <Input
                            type="text"
                            name="latitude"
                            value={latitude}
                            onChange={this.handleInputChange}
                            placeholder="Latitud" />
                        <br />
                        <Label><b>Longitud: </b></Label>
                        <Input
                            type="text"
                            name="longitude"
                            value={longitude}
                            onChange={this.handleInputChange}
                            placeholder="Longitud" />
                        <br />
                        <Label><b>Imagen de perfil: </b></Label>
                        <Input
                            type="file"
                            name="coverUrl"
                            onChange={this.handleChangeImage}
                            placeholder="Foto" 
                            encType = "multipart/form-data"/>
                        <br />
                        <br />
                        <Input
                            type="submit" 
                            value="Dar de alta"/>
                        <br />
                            <Link to="/Clubs">Volver al listado</Link>
                    </Form>
                </Row>
            </>
        )
    }
}
export default FormularioAltaClub;