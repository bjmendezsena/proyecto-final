import React from 'react';
import Club from '../../../modelos/Club';
import { Link, Redirect } from 'react-router-dom';
import { Input, Row, Col, Label, Form, Spinner } from 'reactstrap';
import '../estilos.css';

export default class ModificarClub extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: "",
            name: "",
            streetName: "",
            streetNumber: "",
            postalCode: "",
            city: "",
            ambience: "",
            description: "",
            phone: "",
            dressCode: "",
            coverUrl: "",
            latitude: "",
            longitude: "",
            modificado: 0,
            imgNotFound: "http://46.101.236.229:8080/api/assets/clubs/club.jpg"

        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.guardaClub = this.guardaClub.bind(this);
        this.cargaDatos = this.cargaDatos.bind(this);
        this.handleChangeImage = this.handleChangeImage.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Club.getClubById(this.props.match.params.id * 1)
            .then(club => {
                this.setState({
                    name: club.name,
                    streetName: club.streetName,
                    streetNumber: club.streetNumber,
                    postalCode: club.postalCode,
                    city: club.city,
                    ambience: club.ambience,
                    description: club.description,
                    phone: club.phone,
                    dressCode: club.dressCode,
                    coverUrl: club.coverUrl,
                    latitude: club.latitude,
                    longitude: club.longitude
                });
            })
            .catch(err => console.log(err));
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleChangeImage(evt) {
        console.log("Uploading");
        var reader = new FileReader();
        var file = evt.target.files[0];
        var self = this;

        reader.onload = function (upload) {
            self.setState({
                coverUrl: upload.target.result
            }, function () {
                console.log(self.state.coverUrl);
            });
        };
        reader.readAsDataURL(file);
    }

    guardaClub(e) {
        e.preventDefault();

        let idModificar = this.props.match.params.id * 1;
        let nuevoNombre = this.state.name;
        let nuevoStreetName = this.state.streetName;
        let nuevoStreetNumber = this.state.streetNumber;
        let nuevoAmbience = this.state.ambience;
        let nuevoDescription = this.state.description;
        let nuevoPostalCode = this.state.postalCode;
        let nuevoCity = this.state.city;
        let nuevoPhone = this.state.phone;
        let nuevoDressCode = this.state.dressCode;
        let nuevoCoverUrl = this.state.coverUrl;
        let nuevoLatitude = this.state.latitude;
        let nuevoLongitude = this.state.longitude;

        if (window.confirm("Desea actualizar la información?")) {
            this.setState({ modificado: 1 });
            Club.modificaClub(idModificar, nuevoNombre, nuevoStreetName, nuevoStreetNumber, nuevoAmbience,
                nuevoDescription, nuevoPostalCode, nuevoCity, nuevoPhone, nuevoDressCode,
                nuevoCoverUrl, nuevoLatitude, nuevoLongitude)
                .then(response => {
                    console.log(response);
                    this.setState({ modificado: 2 })
                }
                )
        }
    }

    render() {
        console.log("M", this.state.modificado);
        if (this.state.modificado === 2) {
            return (
                <Redirect to={"../Clubs"} />
            )
        } else if (this.state.modificado === 1) {
            return (<><Spinner color="primary" /> </>)
        }

        if (this.state.coverUrl == null) {
            this.setState({ coverUrl: this.state.imgNotFound })
        }

        return (
            <>
                <Row>
                    <Col className="textAlignCenter">
                        <h2>Editando club: </h2>
                        <br />
                    </Col>
                </Row>
                <Row>
                    <Form onSubmit={this.guardaClub} className="formularioClub">
                        <Col >
                            <Label for="name">Nombre:</Label>
                            <Input value={this.state.name} onChange={this.handleInputChange} type="text" name="name" />
                            <br />

                            <Label for="streetName">Calle:</Label>
                            <Input value={this.state.streetName} onChange={this.handleInputChange} type="text" name="streetName" />
                            <br />

                            <Label for="streetNumber">Número de calle:</Label>
                            <Input value={this.state.streetNumber} onChange={this.handleInputChange} type="number" name="streetNumber" />
                            <br />

                            <Label for="postalCode">Código postal:</Label>
                            <Input value={this.state.postalCode} onChange={this.handleInputChange} type="text" name="postalCode" />
                            <br />

                            <Label for="city">Ciudad:</Label>
                            <Input value={this.state.city} onChange={this.handleInputChange} type="text" name="city" />
                            <br />

                            <Label for="description">Descripción:</Label>
                            <Input value={this.state.description} onChange={this.handleInputChange} type="textarea" name="description" />
                            <br />

                            <Label for="ambience">Ambiente:</Label>
                            <Input value={this.state.ambience} onChange={this.handleInputChange} type="text" name="ambience" />
                            <br />

                            <Label for="phone">Teléfono de contacto:</Label>
                            <Input value={this.state.phone} onChange={this.handleInputChange} type="number" name="phone" />
                            <br />

                            <Label for="dressCode">Código de vestimenta:</Label>
                            <Input value={this.state.dressCode} onChange={this.handleInputChange} type="text" name="dressCode" />
                            <br />

                            <Label for="coverUrl">Imagen:</Label>
                            <br />
                            <img className="imgForm" src={this.state.coverUrl} alt="coverUrl" />
                            <br />
                            <br />

                            <Label for="coverUrl">Modificar imagen:</Label>
                            <br />
                            <Input onChange={this.handleChangeImage} type="file" name="coverUrl" />
                            <br />

                            <Label for="latitude">Latitud:</Label>
                            <Input value={this.state.latitude} onChange={this.handleInputChange} type="text" name="latitude" />
                            <br />

                            <Label for="longitude">Longitud:</Label>
                            <Input value={this.state.longitude} onChange={this.handleInputChange} type="text" name="longitude" />
                            <br />

                            <input className="btn btn-success" type='submit' value="Guardar" />
                            <br />
                            <br />
                            <Link to="/Clubs">Volver al listado</Link>
                        </Col>
                    </Form>
                </Row>
            </>
        )
    }
}