import React from 'react';
import Club from '../../../modelos/Club';
import { Container, Col, Button, Row, Spinner } from 'reactstrap';
//import ButtonComponent from '../../buttonComponent/ButtonComponent';
import { Link, Redirect } from 'react-router-dom';
import '../estilos.css';

class InfoClub extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            name: '',
            streetName: '',
            streetNumber: '',
            postalCode: '',
            city: '',
            description: '',
            ambience: '',
            phone: '',
            dressCode: '',
            coverUrl: '',
            latitude: '',
            longitude: '',
            borrado: 0,
        };
        this.cargaDatos = this.cargaDatos.bind(this);
        this.cargaDatos();
    }

    cargaDatos() {
        Club.getClubById(this.props.match.params.id * 1)
            .then(club => {
                this.setState({
                    id: club.id,
                    name: club.name,
                    streetName: club.streetName,
                    streetNumber: club.streetNumber,
                    postalCode: club.postalCode,
                    city: club.city,
                    description: club.description,
                    ambience: club.ambience,
                    phone: club.phone,
                    dressCode: club.dressCode,
                    coverUrl: club.coverUrl,
                    latitude: club.latitude,
                    longitude: club.longitude,
                });
            })
            .catch(err => console.log(err));
    }

    borrar(idBorrar) {

        if (window.confirm("Desea borrar '" + this.state.name + "'?")) {
            this.setState({ borrado: 1 })
            Club.eliminaClub(idBorrar)
                .then(response => {
                    console.log(response);
                    this.setState({ borrado: 2 })
                })
        }
    }

    render() {

        if (this.state.borrado === 2) {
            return (
                <Redirect to={"../Clubs"} />
            )
        }

        if (this.state.borrado === 1) {
            return (<><Spinner color="primary" /> </>)
        }

        return (
            <>

                <Container>
                    <Row>
                        <Col lx="6" md="6" xs="12">
                            <img className="img-thumbnail animate__animated animate__fadeInLeft"
                                src={this.state.coverUrl}
                                alt={this.state.name}
                            />
                        </Col>
                        <Col lx="6" md="6" xs="12" className="mt-3">
                            <h3> {this.state.name}</h3>
                            <h4 >{this.state.description}</h4>
                        </Col>
                    </Row>
                    <Row>
                        <Col lx="6" md="6" xs="12">
                        </Col>
                        <Col lx="6" md="6" xs="12" className="labelFormularioInfo">
                            <h4>Dirección: {this.state.streetName}, {this.state.streetNumber}, {this.state.city}, {this.state.postalCode}</h4>
                            <h4> </h4>
                            <h4>Vestimenta: {this.state.dressCode}</h4>
                            <h4>Teléfono: {this.state.phone}</h4>
                            {/* <MenuClub/> */}
                        </Col>
                    </Row>
                    <Row className="buttonFormularioInfo">
                        <Col lx="3" md="3" xs="5">
                        </Col>
                        <Col lx="3" md="3" xs="5">
                        </Col>
                        <Col lx="3" md="3" xs="5">
                            <Link to={"/ModificarClub/" + this.state.id}> <Button color="primary">EDITAR</Button></Link>
                        </Col>

                        <Col lx="3" md="3" xs="5">
                            <Button className="mr-1" onClick={() => this.borrar(this.state.id)} color="danger">ELIMINAR</Button>
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
}
export default InfoClub;