import React from 'react';
import Club from '../../../modelos/Club';
import { Link } from 'react-router-dom';
import { Button, Spinner } from 'reactstrap';
import '../estilos.css';

class ListadoClubs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clubs: [],
            sensor: false
        }
    }

    cargaDatos() {
        Club.getClubs()
            .then(data => {
                this.setState({ clubs: data });
                this.setState({ sensor: true });
            })
            .catch(err => console.log(err));
    }

    componentDidMount() {
        console.log("en didMount")
        this.cargaDatos();
    }

    render() {


        if (!this.state.sensor) return <> <h1>Cargando..</h1> <Spinner color="primary" /> </>

        let cardsClubs = this.state.clubs.map(el =>
            <div className="col-sm-8 col-lg-6" key={el.id}>
                <Link to={"/InfoClub/" + el.id} className="linkDecoration">
                    <img className="foto"
                        src={el.coverUrl}
                        alt={el.name}>
                    </img>
                    <h1 className="textAlignLista">{el.name}</h1>
                </Link>
            </div>
        );
        console.log("Termino render");
        return (
            <>
                <br />
                <h1 className="form-group text-center">Listado de clubs
                {"  "}<Link to="/AltaClub" className="link">
                    <br/>
                    <Button color="success">
                        AÑADIR CLUB</Button>
                    </Link>
                </h1>
                <br />

                <div className="row">
                    {cardsClubs}
                </div>
            </>
        )
    }
}
export default ListadoClubs;