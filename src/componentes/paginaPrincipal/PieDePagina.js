import React from 'react';
//Footer
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import 'rc-footer/assets/index.css'; // import 'rc-footer/asssets/index.less';
import '../admins/estilos.css';
import instagram from '../../Images/instagram.png';
import facebook from '../../Images/facebook.png';
import twitter from '../../Images/twitter.ico';
import linkdin from '../../Images/linkdin.png';

class PaginaPrincipal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <div className="footer">
                <Container>
                    <Row>
                        <Col xl="3" md="3" sm="12" >
                            <p><b>Conócenos</b></p>
                            <Link to='' className="colorTextFooter"><p>Sobre Wolpray</p></Link>
                            <Link to='' className="colorTextFooter"><p>Contacta con nosotros</p></Link>
                            <Link to='' className="colorTextFooter"><p>Promociones</p></Link>
                            <br />
                        </Col>
                        <Col xl="3" md="3" sm="12" >
                            <p><b>Por qué Wolpray</b></p>
                            <Link to='' className="colorTextFooter"><p>Principales características</p></Link>
                            <Link to='' className="colorTextFooter"><p>Nuestro punto fuerte</p></Link>
                            <Link to='' className="colorTextFooter"><p>No más aglomeraciones</p></Link>
                            <br />
                        </Col>
                        <Col xl="3" md="3" sm="12" >
                            <p><b>Tipos de negocio</b></p>
                            <Link to='' className="colorTextFooter"><p>Discotecas</p></Link>
                            <Link to='' className="colorTextFooter"><p>Bares de copas</p></Link>
                            <Link to='' className="colorTextFooter"><p>Ocio nocturno</p></Link>
                            <br />
                        </Col>
                        <Col xl="3" md="3" sm="12" className="siguenosFooter">
                            <p><b>Síguenos</b></p>
                            <Link to='' className="colorTextFooter"><p><img src={instagram} alt="instagram" className="imtFooter" /> Instagram</p></Link>
                            <Link to='' className="colorTextFooter"><p><img src={facebook} alt="facebook" className="imtFooter" /> Facebook</p></Link>
                            <Link to='' className="colorTextFooter"><p><img src={twitter} alt="twitter" className="imtFooter" /> Twitter</p></Link>
                            <Link to='' className="colorTextFooter"><p><img src={linkdin} alt="linkdin" className="imtFooter" /> Linkdin</p></Link>
                            <br />
                        </Col>
                        <Col>
                            <hr className="hrFooter"></hr>
                            <p className="copyRightFooter">© 2020 Copyright: Wolpray.com</p>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}
export default PaginaPrincipal;