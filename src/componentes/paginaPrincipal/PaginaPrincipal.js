import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import themeLogo from '../../Images/themeLogo.png';
import servicios from '../../Images/servicios.png';
import flecha from '../../Images/flecha.ico';
import '../admins/estilos.css';
//Carrousel
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
/////////Imagenes Carrousel
import canela from '../../Images/canela.jpg';
import capitolio from '../../Images/capitolio.jpg';
import brisas from '../../Images/brisas.jpg';
import sanpues from '../../Images/sanpues.jpg';
import bunker from '../../Images/bunker.jpg';
// Imagenes Ventajas
import facilUsar from '../../Images/facilUsar.png';
import facilModular from '../../Images/facilModular.png';
import facilGestionar from '../../Images/facilGestionar.png';
import facilDiscotecas from '../../Images/facilDiscotecas.png';
import facilControl from '../../Images/facilControl.png';
import facilAhorraTiempo from '../../Images/facilAhorraTiempo.png';
// Imagenes Google Play y App Store IOS
import googlePlay from '../../Images/googlePlay.png';
import iosStore from '../../Images/iosStore.png';
//Pie de página
import PieDePagina from '../paginaPrincipal/PieDePagina';
//Header
import HeaderComponentInicio from '../headerComponent/HeaderComponentInicio'

class PaginaPrincipal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: null

        }

    }

    render() {

        return (

            <div >

                <HeaderComponentInicio />

                <Container  >
                    <Row>
                        <Col md="6" sm="12" className="mt-5">
                            <br />
                            <h1 className="display-3"><b>Aplicación para discotecas y bares de copas</b></h1>
                            <p className="lead siguenosFooter">Aplicación para facilitar la gestión de discotecas y bares de copas que permite pedir copas desde la mesa, evitando aglomeraciones, gestionar reservas y promocionar tu establecimiento.</p>
                        </Col>
                        <Col md="6" sm="12" className="imgBootstrapPrincipal">
                            <img src={themeLogo} alt="themeLogo" />
                        </Col>
                    </Row>
                </Container>
                <Container  >
                    <Row>
                        <Col xl="7" md="8" sm="12" ><h1 className="display-3 "><b>Principales características de la aplicación</b></h1>
                            <br /><br />
                            <img src={flecha} alt="flecha" className="mt-2 listaPrincipal " />
                            <p className="lead siguenosFooter">   Pedir copas desde la aplicación</p>
                            <img src={flecha} alt="flecha" className="mt-2 listaPrincipal" />
                            <p className="lead siguenosFooter">   Promocionar tu establecimiento.</p>
                            <img src={flecha} alt="flecha" className="mt-2 listaPrincipal" />
                            <p className="lead siguenosFooter">   Gestionar reservas.</p>
                            <img src={flecha} alt="flecha" className="mt-2 listaPrincipal" />
                            <p className="lead siguenosFooter">  Gestionar tus clubs.</p>
                            <img src={flecha} alt="flecha" className="mt-2 listaPrincipal" />
                            <p className="lead siguenosFooter">   Gestionar usuarios.</p>

                        </Col>
                        <Col xl="5" md="4" sm="12" className="mt-5">

                            <img src={servicios} alt="servicios" className="serviciosPrincipal" />
                        </Col>
                    </Row>
                </Container>


                <Container  >
                    <Row>
                        <Col xs="12" className="ventajasDescripcionPrincipal">
                            <br />
                            <h1 className="display-3"><b>Conoce las ventajas de la aplicación Wolpray</b></h1>

                        </Col>
                    </Row>
                    <Row className="ventajasDescripcionPrincipal">

                        <Col md="4" sm="12">
                            <img src={facilUsar} alt="facilUsar" className="ventajasPrincipal" />
                            <h5 className="mt-3">Uso</h5><br />
                            <p>Intuitiva y fácil de usar por cualquier usuario.</p>
                        </Col>

                        <Col md="4" sm="12">
                            <img src={facilModular} alt="facilModular" className="ventajasPrincipal" />
                            <h5 className="mt-3">Modular</h5><br />
                            <p>Posibilidad de ampliar funcionalidades bajo demanda.</p>
                        </Col>

                        <Col md="4" sm="12">
                            <img src={facilGestionar} alt="facilGestionar" className="ventajasPrincipal" />
                            <h5 className="mt-3">Gestión</h5><br />
                            <p>Facilidad para gestionar usuarios, clubs, productos y todo lo que quieras.</p>
                        </Col>
                    </Row>
                    <Row className="ventajasDescripcionPrincipal">

                        <Col md="4" sm="12">
                            <img src={facilDiscotecas} alt="facilDiscotecas" className="ventajasPrincipal" />
                            <h5 className="mt-3">Ocio nocturno</h5><br />
                            <p>Pensado y creado para discotecas y bares de copas</p>
                        </Col>

                        <Col md="4" sm="12">
                            <img src={facilControl} alt="facilControl" className="ventajasPrincipal" />
                            <h5 className="mt-3">Control</h5><br />
                            <p>Facilidad de controlar todo lo que sucede en tu club.</p>
                        </Col>

                        <Col md="4" sm="12">
                            <img src={facilAhorraTiempo} alt="facilAhorraTiempo" className="ventajasPrincipal" />
                            <h5 className="mt-3">Tiempo</h5><br />
                            <p>Permite ahorrar tiempo.</p>
                        </Col>
                    </Row>
                </Container>

                <Container>
                    <Carousel infiniteLoop useKeyboardArrows autoPlay className="mt-5 carrusel">
                        <div>
                            <img src={canela} alt="canela" />
                        </div>
                        <div>
                            <img src={capitolio} alt="capitolio" />
                        </div>
                        <div>
                            <img src={brisas} alt="brisas" />
                        </div>
                        <div>
                            <img src={sanpues} alt="sanpues" />
                        </div>
                        <div>
                            <img src={bunker} alt="bunker" />
                        </div>
                    </Carousel>
                </Container>

                <Container>
                    <Row>

                        <Col xs="12" className="textAlignCenter mt-5" >
                            <h1 className="display-3"><b>Prueba Wolpray</b> <b className="gratisPrincipal">gratis</b></h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col xl="6" md="7" sm="12">
                            <Link to="/">
                                <img src={googlePlay} alt="GooglePlay" className="appPrincipal" />
                            </Link>
                        </Col>
                        <Col xl="6" md="7" sm="12" >
                            <Link to="/">
                                <img src={iosStore} alt="iosStore" className="appPrincipal" />
                            </Link>
                        </Col>

                    </Row>
                </Container>
                <PieDePagina />
            </div>
        )
    }
}
export default PaginaPrincipal;