import React, { Component } from 'react';
import ButtonComponent from '../buttonComponent/ButtonComponent';
import ServicioPrueba from '../managers/model/ServiciosPrueba';
import './login.css';
import themeLogo from '../../Images/themeLogo.png';
//Header
import HeaderComponentInicio from '../headerComponent/HeaderComponentInicio'
//Pie de página

class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            user: null,
            admin: false,
            manager: false
        }
        console.log("DESDE LOGINSCREEN");
        console.log(this.state.password);
    }


    onSubmitInput = (e) => {
        e.preventDefault();
        //"/principalmanagers/club/:idClub"
        ServicioPrueba.lloginUser(this.state)
            .then(resp => {
                this.setState({ user: resp })
                if (this.state.user === 'Correo electrónico o contraseña incorrecta') {
                    alert(resp)
                } else if (this.state.user != null) {
                    if (this.state.email === this.state.user.mail && this.state.password === this.state.user.password) {
                        localStorage.setItem('user', JSON.stringify(this.state.user));
                        if (this.state.user.role === 'admin') {
                            this.setState({
                                admin: true,
                                manager: false
                            })
                            this.props.history.push(`/Clubs`, this.state.user);
                            console.log("login admin")
                        }
                        if (this.state.user.role === 'manager') {
                            this.setState({
                                admin: false,
                                manager: true
                            })
                            this.props.history.push(`/principalmanagers/${this.state.user.id}`, this.state.user);
                            console.log("login manager")
                        }
                    } else {
                        alert("El email o la contraseña esta mal");
                    }

                } else {
                    alert("No existe este usuario");
                }
            });
        console.log(this.state.email);
        console.log(this.state.password);
    }

    handleInputChange = ({ target }) => {
        this.setState(
            {
                ...this.state,
                [target.name]: target.value
            }
        );
        console.log("DESDE handleInputChange")
        console.log(this.state)
    }

    render() {
        const { email, password } = this.state;

        return (<>
            <HeaderComponentInicio />

            <form className="login-bx mt-5" onSubmit={this.onSubmitInput}>
                <h1>Login</h1>
                <div className="bximg">
                    <img src={themeLogo} />
                </div>

                <div className="txtb">
                    <input
                        type="email"
                        name="email"
                        value={email}
                        onChange={this.handleInputChange}
                        autoComplete="off"
                    />
                    <span placeholder="Correo electrónico"></span>
                </div>

                <div className="txtb">
                    <input
                        type="password"
                        name="password"
                        value={password}
                        onChange={this.handleInputChange}
                        autoComplete="off"
                    />
                    <span placeholder="Contraseña"></span>
                </div>

                <div onClick={this.onSubmitInput}>
                    <ButtonComponent text="Iniciar sesion" />
                </div>

                <div className="bottom-text">
                    ¿Aún no eres parte de nosotros? <a href="#">¡Contáctanos!</a>
                </div>
            </form>


            {/* <PieDePagina /> */}
        </>);
    }
}
export default LoginScreen;