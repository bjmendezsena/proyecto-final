import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import WolprayApp from './WolprayApp';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';


ReactDOM.render(
    <WolprayApp />,
  document.getElementById('root')
);

