const APIURL = ' http://46.101.236.229:8080/api/'; 
const SERVLET = 'reservationservlet';

export default class Reservas {

    static getReserva = () => {
        let url = APIURL + SERVLET;
        console.log("GET: ", url)
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(datos => resolve(datos.data))
                .catch(err => reject(err));
        });
    }

    static getReservaById = (idSolicitado) => {
        let url = APIURL + SERVLET + "/" + idSolicitado;
        console.log("GetById: ", url)
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getReservaByClub = (idSolicitado) => {
        let url = APIURL + SERVLET + "/club/" + idSolicitado;
        console.log("GetByClub: ", url)
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getReservaByDate = (idSolicitado) => {
        let url = APIURL + SERVLET + "/date/" + idSolicitado;
        console.log("GetByDate: ", url)
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getReservaByUser = (idSolicitado) => {
        let url = APIURL + SERVLET + "/user/" + idSolicitado;
        console.log("GetByUser: ", url)
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getReservaByStatus = (idSolicitado) => {
        let url = APIURL + SERVLET + "/" + idSolicitado;
        console.log("GetByStatus: ", url)
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static deleteReserva = (idBorrar,) => {
        let url = APIURL + SERVLET + "/" + idBorrar;
        console.log("DELETE: ", url)
        return this.envia('DELETE', url);
    }

    static editReserva = (idEdit, clubId, userId, n_people, notes, date, status) => {
        let datos = {
            clubId: clubId,
            userId: userId,
            n_people: n_people,
            notes: notes,
            date: date,
            status: status
        }
        console.log(datos);
        let url = APIURL + SERVLET + "/" + idEdit;
        console.log("PUT: ", url)
        return this.envia('PUT', url, datos);
    }

    static envia(method, url, datos = "") {
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        console.log(method, url);

        return new Promise((resolve, reject) => {
            fetch(proxyurl + url, {
                method: method,
                headers: new Headers({ 'Content-Type': 'application/json' }),
                body: JSON.stringify(datos)
            })
                .then(response => resolve(response))
                .catch(err => reject(err()));
        })
    }
}