//import PRODUCTOS from '../componentes/managers/model/products/products.json';
const APIURL = 'http://46.101.236.229:8080/api/';
const SERVLET = 'orderservlet';
//productservlet?action=

export default class Productos {

    static getPedido = () => {
        let url = APIURL + SERVLET;
        console.log("GET: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(datos => resolve(datos.data))
                .catch(err => reject(err));
        });
    };

    static getPedidoById = (idSolicitado) => {
        let url = APIURL + SERVLET + "/" + idSolicitado;
        console.log("GetById: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getPedidoByClub = (idSolicitado) => {
        let url = APIURL + SERVLET + "/club/" + idSolicitado;
        console.log("GetByClub: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getPedidoByPromocion = (idSolicitado) => {
        let url = APIURL + SERVLET + "/promotion/" + idSolicitado;
        console.log("GetBypromocion: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getPedidoByUser = (idSolicitado) => {
        let url = APIURL + SERVLET + "/user/" + idSolicitado;
        console.log("GetByUser: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getPedidoByProduct = (idSolicitado) => {
        let url = APIURL + SERVLET + "/product/" + idSolicitado;
        console.log("GetByProduct: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getPedidoByStatus = (idSolicitado) => {
        let url = APIURL + SERVLET + "/" + idSolicitado;
        console.log("GetByStatus: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static addPedido = (clubId, userId, productId, promotionId, price, status) => {
        let datos = {
            clubId: clubId,
            userId: userId,
            productId: productId,
            promotionId: promotionId,
            price: price,
            status: status
        }
        console.log(datos);
        let url = APIURL + SERVLET;
        console.log("POST: ", url);
        return this.envia('POST', url, datos);;
    }

    static deletePedido = (idBorrar) => {
        let url = APIURL + SERVLET + "/" + idBorrar;
        console.log("DELETE: ", url);
        return this.envia('DELETE', url);
    }

    static editPedido = (idEdit, clubId, userId, productId, promotionId, price, status) => {
        let datos = {
            clubId: clubId,
            userId: userId,
            productId: productId,
            promotionId: promotionId,
            price: price,
            status: status
        }
        console.log(datos);
        let url = APIURL + SERVLET + "/" + idEdit;
        console.log("PUT: ", url);
        return this.envia('PUT', url, datos);
    }

    static envia(method, url, datos = "") {
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        console.log(method, url);

        return new Promise((resolve, reject) => {
            fetch(proxyurl + url, {
                method: method,
                headers: new Headers({ 'Content-Type': 'application/json' }),
                body: JSON.stringify(datos)
            })
                .then(response => resolve(response))
                .catch(err => reject(err()));
        })
    }
}
