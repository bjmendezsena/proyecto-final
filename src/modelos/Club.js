const APIURL = 'http://46.101.236.229:8080/api/'
const SERVLET = 'clubservlet/'

class Club {
    static getClubs = () => {
        let url = APIURL + SERVLET;
      
        console.log("GET: ", url)
      
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(datos => resolve(datos.data))
                .catch(err => reject(err));
        });
    }

    static getClubById = (idSolicitado) => {//Esta función hace un get de UN SOLO CLUB. Es la API quien ha de proporcionar el club concreto en función del idSolicitado.
        let url = APIURL + SERVLET + idSolicitado;
      
        console.log("GetByID: ", url);
      
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(datos => resolve(datos.data))
                .catch(err => reject(err));
        });
    }

    static addClub = (clubName, streetName, streetNumber, postalCode, city, description, ambience, phone, dressCode, latitude, longitude, coverUrl) => {

        let datos = {//ESTOS SON LOS CAMPOS CUYOS NOMBRES TIENEN QUE COINCIDIR CON LA BDD!!!!
            name: clubName,
            streetName: streetName,
            streetNumber: streetNumber,
            postalCode: postalCode,
            city: city,
            description: description,
            ambience: ambience,
            phone: phone,
            dressCode: dressCode,
            latitude: latitude,
            longitude: longitude,
            coverUrl: coverUrl
        }

        console.log(datos);
        let url = APIURL + SERVLET;
        console.log("POST: ", url)
        return this.envia('POST', url, datos);
    }

    static eliminaClub = (idBorrar) => {
        let url = APIURL + SERVLET + idBorrar;
        console.log("DELETE: ", url)

       return this.envia('DELETE', url);
    }

    static modificaClub = (idModificar, clubName, streetName, streetNum, ambience, description, postalCode, city, phone,
        dressCode, coverUrl, latitude, longitude) => {

        let datos = {
            name: clubName,
            streetName: streetName,
            streetNum: streetNum,
            postalCode: postalCode,
            city: city,
            description: description,
            ambience: ambience,
            phone: phone,
            dressCode: dressCode,
            coverUrl: coverUrl,
            latitude: latitude,
            longitude: longitude,
        }

        console.log(datos);
        let url = APIURL + SERVLET + idModificar;
        
       return this.envia('PUT', url, datos);
    }

    static envia(method, url, datos=""){
        //const proxyurl = "https://cors-anywhere.herokuapp.com/";
        console.log(method, url);

        return new Promise((resolve, reject) => {
            fetch(url, {
                method: method,
                headers: new Headers({ 'Content-Type' : 'application/json' }),
                body: JSON.stringify(datos) 
            })
                .then(response => resolve(response))
                .catch(err => reject(err()));
        })
    }
}
export default Club;