const APIURL = 'http://46.101.236.229:8080/api/';
const SERVLET = 'userservlet/'

class Usuario {
    static getUsuarios = () => {
        let url = APIURL + SERVLET;
        console.log("GET: ",url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(datos => resolve(datos.data))
                .catch(err => reject(err));
        });
    }; 

    static getUsuarioById = (idSolicitado) => {//Esta función hace un get de UN SOLO USUARIO. Es la API quien ha de proporcionar el usuario concreto en función del idSolicitado
        let url = APIURL + SERVLET + idSolicitado;
        console.log("URL:", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(datos => resolve(datos.data))
                .catch(err => reject(err));
        });
    }

    static addUsuario = (userName, mail, password, role, phone, birthDate) => {

        let datos = {//ESTOS SON LOS CAMPOS CUYOS NOMBRES TIENEN QUE COINCIDIR CON LA BDD!!!!
            userName: userName,
            mail: mail,
            password: password,
            role: role,
            phone: phone,
            birthDate: birthDate
        }
        
        console.log(datos);
        let url = APIURL + SERVLET;
        console.log("POST: ",url);

       return this.envia('POST', url, datos);
    }

    static eliminaUsuario = (idBorrar) => {
        
        let url = APIURL + SERVLET + idBorrar;
        console.log("DELETE: ",url);
        console.log(url);
        
       return this.envia('DELETE', url);
    }

    static modificaUsuario = (idModificar, nombreModificar, correoModificar, passwordModificar,
         telefonoModificar, fechaNacimientoModificar) => {
        
        let datos = { 
            userName: nombreModificar,
            mail: correoModificar, 
            password: passwordModificar,
            phone: telefonoModificar, 
            birthDate: fechaNacimientoModificar
        }

        console.log(datos);
        let url = APIURL + SERVLET + idModificar;
        console.log("PUT: ",url);

        return this.envia('PUT', url, datos);
    }

    static envia(method, url, datos=""){
        //const proxyurl = "https://cors-anywhere.herokuapp.com/";
        console.log(method, url);
        
        return new Promise((resolve, reject) => {
            fetch(url, {
                method: method,
                headers: new Headers({ 'Content-Type' : 'application/json' }),
                body: JSON.stringify(datos) 
            })
                .then(response => resolve(response))
                .catch(err => reject(err()));
        })
    }
}
export default Usuario;