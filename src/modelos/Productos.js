//import PRODUCTOS from '../componentes/managers/model/products/products.json';
const APIURL = 'http://46.101.236.229:8080/api/';
const SERVLET = 'productservlet';
//http://46.101.236.229:8080/api/productservlet/3
//productservlet?action=

export default class Productos {

    static getProducto = () => {
        let url = APIURL + SERVLET;
        console.log("GET: ", url)
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(datos => resolve(datos.data))
                .catch(err => reject(err));
        });
    };

    static getProductoById = (idSolicitado) => {
        let url = APIURL + SERVLET + "/" + idSolicitado;
        console.log("GetById: ", url)
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getProductoByClub = (idSolicitado) => {
        let url = APIURL + SERVLET + "/club/" + idSolicitado;
        console.log("GetByClub: ", url)
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))

                .catch(err => reject(err));
        });
    }

    static getProductoByPromotion = (idSolicitado) => {
        let url = APIURL + SERVLET + "/promotion/" + idSolicitado;
        console.log("GetByPromotion: ", url)
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getPromocionByMorePrice = (idSolicitado) => {
        let url = APIURL + SERVLET + "/morethan/" + idSolicitado;
        console.log("GetByMorePric: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getPromocionByLessPrice = (idSolicitado) => {
        let url = APIURL + SERVLET + "/lessthan/" + idSolicitado;
        console.log("GetByLessPrice: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getPromocionByCategory = (idSolicitado) => {
        let url = APIURL + SERVLET + "/category/" + idSolicitado;
        console.log("GetByCategory: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static addProducto = (clubId, name, category, description, price, imageUrl, status) => {
        let datos = {
            clubId: clubId,
            name: name,
            category: category,
            description: description,
            price: price,
            imageUrl: imageUrl,
            status: status
        }
        console.log(datos);
        let url = APIURL + SERVLET + "/" + clubId;
        console.log("POST: ", url)
        return this.envia('POST', url, datos);
    }

    static deleteProducto = (idBorrar) => {
        let url = APIURL + SERVLET + "/" + idBorrar;
        console.log("DELETE: ", url)
        return this.envia('DELETE', url);
    }

    static editProducto = (idEdit, clubId, name, category, description, price, coverUrl, status) => {
        let datos = {
            clubId: clubId,
            name: name,
            category: category,
            description: description,
            price: price,
            coverUrl: coverUrl,
            status: status
        }
        console.log(datos);
        let url = APIURL + SERVLET + "/" + idEdit;
        console.log(url);
        return this.envia('PUT', url, datos);
    }

    static envia(method, url, datos = "") {
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        console.log(method, url);

        return new Promise((resolve, reject) => {
            fetch(proxyurl + url, {
                method: method,
                headers: new Headers({ 'Content-Type': 'application/json' }),
                body: JSON.stringify(datos)
            })
                .then(response => resolve(response))
                .catch(err => reject(err()));
        })
    }
}
