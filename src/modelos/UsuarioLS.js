import USUARIOS from '../componentes/admins/UsuarioComponents/users.json';
const APIURL = 'http://46.101.236.229:8080/api/userservlet?action='
const SERVLET = 'userservlet/'
let CLAVE_USUARIOS = "wolpray_usuarios";

/**LAS FUNCIONES QUE COMIENZAN CON ____ SON PARA TRABAJAR EN LOCAL, SIN CONEXIÓN CON BDD*/

class Usuario {
    static ______getUsuarios = () => {//return this.recuperar() para trabajar con localStorage y return CLUBS.data para trabajar desde JSON o desde BDD.
        return this.recuperar();
        //return USUARIOS.data;
    }

    static ______getUsuarioById = (idUsuario) => {//Esta función es capaz de proporcionar UN SOLO USUARIO en funcion del idClub. Es tarea de la API el proporcionar un usuario concreto en función de un id, no del front.
        /**Para utilizar la BDD hay que cambiar el this.recuperar(); por CLUBS.data; */
        //let usuarios = this.recuperar();
        let usuarios = USUARIOS.data;
        let elUsuario = usuarios.find(el => el.id === idUsuario * 1);
        return elUsuario;
    }

    static ____addUsuario = (name, mail, password, role, phone, birthDate) => {
        let usuarios = this.recuperar();

        let max = 0;
        usuarios.forEach(el => {
            max = max < el.id ? el.id : max;
        })
        let nuevoId = max + 1;
        let usuarioNuevo = {
            id: nuevoId,
            name: name,
            mail: mail,
            password: password,
            role: role,
            phone: phone,
            birthDate: birthDate,
        };
        usuarios.push(usuarioNuevo);
        this.guardar(usuarios);
    }

    static ____eliminaUsuario = (idBorrar) => {
        let usuarios = this.recuperar();
        console.log(idBorrar);
        if (window.confirm("Desea borrar '" + this.getUsuarioById(idBorrar).userName + "'?")) {
            usuarios = usuarios.filter(el => el.id !== idBorrar);
        }
        this.guardar(usuarios);
    }

    static ____modificaUsuario(idModificar, correoModificar, passwordModificar,
        roleModificar, telefonoModificar, fechaNacimientoModificar) {
        let usuarios = this.recuperar();
        usuarios = usuarios.map(el => {
            if (el.id === idModificar * 1) {
                el.mail = correoModificar;
                el.password = passwordModificar;
                el.role = roleModificar;
                el.phone = telefonoModificar;
                el.birthDate = fechaNacimientoModificar;
            }
            return el;
        });
        this.guardar(usuarios);
    }

    /**Las funciones de guardar y de recuperar SOLO SIRVEN PARA TRABAJAR DESDE LOCALSTORAGE*/
    static guardar(lista) {
        localStorage.setItem(CLAVE_USUARIOS, JSON.stringify(lista));
    }

    static recuperar() {
        let usuarios = JSON.parse(localStorage.getItem(CLAVE_USUARIOS));
        if (usuarios !== null) {
            return usuarios;
        } else {
            return [];
        }
    }
/**------------------------------------------------------------------------------------ */
}
export default Usuario;