const APIURL = 'http://46.101.236.229:8080/api/';
const SERVLET = 'promotionservlet';

export default class Promociones {

    static getPromocion = () => {
        let url = APIURL + SERVLET;
        console.log("GET: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(datos => resolve(datos.data))
                .catch(err => reject(err));
        });
    }

    static getPromocionById = (idSolicitado) => {
        let url = APIURL + SERVLET + "/" + idSolicitado;
        console.log("GetById: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static getPromocionByClub = (idSolicitado) => {
        let url = APIURL + SERVLET + "/club/" + idSolicitado;
        console.log("GetByClub: ", url);
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }

    static addPromocion = (clubId, name, description, coverUrl) => {
        let datos = {
            clubId: clubId,
            name: name,
            description: description,
            coverUrl: coverUrl
        }
        console.log(datos);
        let url = APIURL + SERVLET + "/" + clubId;
        console.log("POST: ", url)
        return this.envia('POST', url, datos);
    }

    static deletePromocion = (idBorrar) => {
        let url = APIURL + SERVLET + "/" + idBorrar;
        console.log("DELETE: ", url)
        return this.envia('DELETE', url);
    }

    static editPromocion = (idEdit, clubId, name, description, coverUrl) => {
        let datos = {
            clubId: clubId,
            name: name,
            description: description,
            coverUrl: coverUrl
        }
        console.log(datos);
        let url = APIURL + SERVLET + "/" + idEdit;
        console.log("PUT: ", url)
        return this.envia('PUT', url, datos);
    }

    static envia(method, url, datos = "") {
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        console.log(method, url);

        return new Promise((resolve, reject) => {
            fetch(proxyurl + url, {
                method: method,
                headers: new Headers({ 'Content-Type': 'application/json' }),
                body: JSON.stringify(datos)
            })
                .then(response => resolve(response))
                .catch(err => reject(err()));
        })
    }
}