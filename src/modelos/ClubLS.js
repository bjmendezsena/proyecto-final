import CLUBS from '../componentes/admins/ClubComponents/clubs.json';
const APIURL = 'http://46.101.236.229:8080/api/'
const SERVLET = 'clubservlet/'
let CLAVE_CLUBS = "wolpray_clubs";

/**LAS FUNCIONES QUE COMIENZAN CON ____ SON PARA TRABAJAR EN LOCAL, SIN CONEXIÓN CON BDD*/

class Club {
    static ___getClubs = () => {//return this.recuperar() para trabajar con localStorage y return CLUBS.data para trabajar desde JSON.
        return this.recuperar();
        //return CLUBS.data;
    }

    static _____getClubById = (idClub) => {//Esta función es capaz de proporcionar UN SOLO CLUB en función del idClub.
        /**Para utilizar un JSON hay que cambiar el this.recuperar(); por CLUBS.data; */
        //let clubs = this.recuperar();
        let clubs = CLUBS.data;
        let elClub = clubs.find(el => el.id === idClub * 1);
        return elClub;
    }
    
    static ___addClub = (name, streetName, streetNumber, postalCode, city, description, ambience, phone, dressCode, coverUrl, latitude, longitude) => {
        let clubs = this.recuperar();

        let max = 0;
        clubs.forEach(el => {
            max = max < el.id ? el.id : max;
        })
        let nuevoId = max + 1;
        let clubNuevo = {
            id: nuevoId,
            name: name,
            streetName: streetName,
            streetNumber: streetNumber,
            postalCode: postalCode,
            city: city,
            description: description,
            ambience: ambience,
            phone: phone,
            dressCode: dressCode,
            coverUrl: coverUrl,
            latitude: latitude,
            longitude: longitude
        };
        clubs.push(clubNuevo);
        this.guardar(clubs);
    }

    static ___eliminaClub = (idBorrar) => {
        let clubs = this.recuperar();
        if (window.confirm("Desea borrar '" + this.getClubById(idBorrar).name + "'?")) {
            clubs = clubs.filter(el => el.id !== idBorrar);
        }
        this.guardar(clubs);
    }

    static ___modificaClub(idModificar, descripcionModificar){
        let clubs = this.recuperar();
        clubs = clubs.map(el => {
            if (el.clubID===idModificar*1){
                el.descripcion = descripcionModificar;
            }
            return el;
        })
        this.guardar(clubs);
    }

    /**Las funciones de guardar y de recuperar SOLO SIRVEN PARA TRABAJAR DESDE LOCALSTORAGE*/
    static guardar(lista) {
        localStorage.setItem(CLAVE_CLUBS, JSON.stringify(lista));
    }

    static recuperar() {
        let clubs = JSON.parse(localStorage.getItem(CLAVE_CLUBS));
        if (clubs !== null) {
            return clubs;
        } else {
            return [];
        }
    }
/**------------------------------------------------------------------------------------ */
}
export default Club;